<?php

function role_list()
{
    global $app;
    $args_twig = [
        'tab_col' => role_colonnes(),
        'tab_filtres' => role_filtres()
    ];
    return $app['twig']->render(fichier_twig(), $args_twig);
}


function role_colonnes()
{

    $tab_colonne = array();
    $tab_colonne['id'] = [];
    $tab_colonne['name'] = [];
    $tab_colonne['permission'] = [];
    $tab_colonne['action'] = ["orderable" => false];
    $tab_colonne = datatable_complete_liste_colonne($tab_colonne);
    return $tab_colonne;
}


function role_filtres()
{
    global $app;
    $request = $app['request'];
    $rechercher = $request->get('search');
    $tab_filtres['premier']['search'] = filtre_ajouter_recherche($rechercher['value']);
    return $tab_filtres;
}


function action_role_list_dataliste()
{
    return objet_liste_dataliste('role');
}


function action_role_list_rechercher()
{
    return objet_liste_rechercher('role');
}

