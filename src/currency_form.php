<?php


use Symfony\Component\Form\FormError as FormError;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Pasteque\Admin\Model\Currency;
use Pasteque\Admin\Forms\CurrencyForm;


function currency_form()
{
    global $app;
    $args_rep = [];
    $request = $app['request'];
    $id = sac('id');
    $data = [];
    if ($id) {
        $modification = true;
        $currency = charger_objet();
        $data = $currency->toArray();

    } else {
        $modification = false;
        $data = ['rate'=>0];

    }
    $builder = $app['form.factory']->createNamedBuilder('currency',CurrencyForm::class,$data);
    $builder->setRequired(false);
    formSetAction($builder,$modification);

    $form = $builder->add('submit',SubmitType::class,
        array('label' => $app->trans('Save'), 'attr' => array('class' => 'btn-primary')))
        ->getForm();


    $form->handleRequest($request);
    if ($form->isSubmitted()) {
        $data_form = $form->getData();
        if ($form->isValid()) {

            if (!$modification){
                $currency = new currency();
            }

            $currency->fromArray($data_form);
            list($reponse, $statut, $err) = appelAPI('/api/currency',[],$currency->toStruct(),[],'PUT');
            if ($statut==200){
                $app['orm.em']->persist($currency);
                $app['orm.em']->flush();
            }

        }
    }
    $args_rep['js_init'] = 'currency_form';
    return reponse_formulaire($form,$args_rep);

}


function action_currency_form_supprimer(){
    return action_supprimer_une_instance();
}
