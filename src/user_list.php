<?php

function user_list()
{
    global $app;
    $args_twig = [
        'tab_col' => user_colonnes(),
        'tab_filtres' => user_filtres()
    ];
    return $app['twig']->render(fichier_twig(), $args_twig);
}


function user_colonnes()
{

    $tab_colonne = array();
    $tab_colonne['id'] = ['title' => 'id'];
    $tab_colonne['image'] = ['title' => 'image'];
    $tab_colonne['name'] = [];
    $tab_colonne['role.name'] = [];
    $tab_colonne['active'] = [];
    $tab_colonne['action'] = ["orderable" => false];
    $tab_colonne = datatable_complete_liste_colonne($tab_colonne);
    return $tab_colonne;
}


function user_filtres()
{
    global $app;
    $request = $app['request'];
    $rechercher = $request->get('search');
    $tab_filtres['premier']['search'] = filtre_ajouter_recherche($rechercher['value']);
    return $tab_filtres;
}


function action_user_list_dataliste()
{
    return objet_liste_dataliste('user');
}


function action_user_list_rechercher()
{
    return objet_liste_rechercher('user');
}

