<?php


use Symfony\Component\Form\FormError as FormError;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Pasteque\Admin\Model\Customer;
use Pasteque\Admin\Forms\CustomerForm;


function customer_form()
{
    global $app;
    $args_rep = [];
    $request = $app['request'];
    $id = sac('id');
    $data = [];
    if ($id) {
        $modification = true;
        $customer = charger_objet();
        $data = $customer->toArray();

    } else {
        $modification = false;
    }
    $builder = $app['form.factory']->createNamedBuilder('customer',CustomerForm::class,$data);
    $builder->setRequired(false);
    formSetAction($builder,$modification);

    $form = $builder->add('submit',SubmitType::class,
        array('label' => $app->trans('Save'), 'attr' => array('class' => 'btn-primary')))
        ->getForm();


    $form->handleRequest($request);
    if ($form->isSubmitted()) {
        $data_form = $form->getData();
        if ($form->isValid()) {

            if (!$modification){
                $customer = new customer();
            }

            $customer->fromArray($data_form);

            if (!$modification){
                list($reponse, $statut, $err) = appelAPI('/api/product',[],$customer->toStruct(),[],'POST');
            }
            else{
                 list($reponse, $statut, $err) = appelAPI('/api/product',[],$customer->toStruct(),[],'POST');
            }
            if ($statut==200){
                $app['orm.em']->persist($customer);
                $app['orm.em']->flush();
            }

        }
    }

    return reponse_formulaire($form,$args_rep);

}


function action_customer_form_supprimer(){
    return action_supprimer_une_instance();
}
