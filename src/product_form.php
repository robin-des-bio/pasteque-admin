<?php


use Symfony\Component\Form\FormError as FormError;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Pasteque\Admin\Model\Product;
use Pasteque\Admin\Forms\ProductForm;


function product_form()
{
    global $app;
    $args_rep = [];
    $request = $app['request'];
    $id = sac('id');
    $data = [];
    if ($id) {
        $modification = true;
        $product = charger_objet();
        $data = $product->toArray();

    } else {
        $modification = false;
        $data = ['discountRate'=>0];

    }
    $builder = $app['form.factory']->createNamedBuilder('product',ProductForm::class,$data);
    $builder->setRequired(false);
    formSetAction($builder,$modification,['id'=>$id]);

    $form = $builder->add('submit',SubmitType::class,
                array('label' => $app->trans('Save'), 'attr' => array('class' => 'btn-primary')))
            ->getForm();


    $form->handleRequest($request);
    if ($form->isSubmitted()) {
        $data_form = $form->getData();
        if ($form->isValid()) {

            if (!$modification){
                $product = new Product();
            }
            else
            {
                $reference_origine = $product->getReference();
            }

            $product->fromArray($data_form);

            if (!$modification){
                list($reponse, $statut, $err) = appelAPI('/api/product',[],$product->toStruct(),[],'POST');
            }
            else{
                $pr = $product->toStruct();
                // TODO : Passer en méthode PATCH
                //unset($pr['id']);
                //list($reponse, $statut, $err) = appelAPI('/api/product/'.$reference_origine,[],$pr,[],'PATCH');
                list($reponse, $statut, $err) = appelAPI('/api/product',[],$pr,[],'POST');
            }
            if ($statut==200){
                $app['orm.em']->persist($product);
                $app['orm.em']->flush();
            }

        }
    }
    $args_rep['js_init'] = 'product_form';
    return reponse_formulaire($form,$args_rep);

}


function action_product_form_supprimer(){
    return action_supprimer_une_instance();
}