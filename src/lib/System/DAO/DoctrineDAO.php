<?php
//    Pastèque API
//
//    Copyright (C) 
//			2012 Scil (http://scil.coop)
//			2017 Karamel, Association Pastèque (karamel@creativekara.fr, https://pasteque.org)
//
//    This file is part of Pastèque.
//
//    Pastèque is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Pastèque is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Pastèque.  If not, see <http://www.gnu.org/licenses/>.

namespace Pasteque\Admin\System\DAO;

use \Doctrine\Common\Collections\Criteria;
use \Doctrine\ORM\EntityManager;
use \Doctrine\ORM\Tools\Setup;

class DoctrineDAO implements DAO
{
    protected $em;
    /** Duplicated entity manager required to fuck off the in-memory cache. */
    protected $roEm;
    protected $inTransaction;
    /** Holds the ids of models deleted during the transaction and not commited.
     * delete/read without commit makes Doctrine go in a deadlock.
     * Contains the deleted model indexed by the full ID string.
     * Emptied on commit. */
    protected $deletedIds;

    private static function getDriverName($type) {
        switch (strtolower($type)) {
            case 'mysql': return 'pdo_mysql';
            case 'postgresql': return 'pdo_pgsql';
            case 'sqlite': return 'pdo_sqlite';
            default: throw new DBException(sprintf('Unsupported database type %s', $type), DBException::CODE_TYPE_ERROR);
        }
    }

    /** Get a DoctrineDAO. It does not try to connect to the database directly.
     * @throw DBException if the type is unknown or unsupported. */
    public function __construct($dbInfo, $options = array()) {
        $path = [__DIR__ . '/../../Model'];
        $isDevMode = (isset($options['debug'])) ? $options['debug'] : false;
        if (!array_key_exists('type', $dbInfo)) {
            throw new DBException('Not database type provided', DBException::CODE_TYPE_ERROR);
        }
        $driver = DoctrineDAO::getDriverName($dbInfo['type']);
        $dbParams = array('dbname' => $dbInfo['name'],
                'user' => $dbInfo['user'],
                'password' => $dbInfo['password'],
                'host' => $dbInfo['host'],
                'driver' => $driver,
                'charset'=> 'utf8');
        $setup = Setup::createAnnotationMetadataConfiguration($path, $isDevMode);

        $this->em = EntityManager::create($dbParams, $setup);
        $this->roEm = EntityManager::create($dbParams, $setup);
        $this->inTransaction = false;
        $this->deletedIds = [];
    }

    /** Get a full ID string for delete/read to use with deletedIds.
     * Has 2 signatures: getIdKey($model) and getIdKey($class, $id). */
    private function getIdKey() {
        $argc = func_get_args();
        if ($argc === 1) { return $model->getClass() . '__' . $model->getId(); }
        else if ($argc === 2) { return $model . '__' . $id; }
        else { return null; }
    }

    private function startTransaction() {
        $this->em->beginTransaction();
        $this->inTransaction = true;
    }

    public function commit() {
        if (!$this->inTransaction) { return; }
        try {
            $this->deletedIds = array();
            $this->em->flush();
            $this->em->commit();
            $this->inTransaction = false;
        } catch (\Exception $e) {
            // Reopen entity manager if it was closed because of a DB failure.
            if (!$this->em->isOpen()) {
                $this->em = $this->em->create($this->em->getConnection(), $this->em->getConfiguration());
            }
            $this->em->rollback();
            $this->inTransaction = false;
            throw $e;
        }
    }

    public function write($model) {
        if (!$this->inTransaction) { $this->startTransaction(); }
        $this->em->persist($model);
    }

    public function read($modelName, $id) {
        if ($id === null) { return null; }
        return $this->em->find($modelName, $id);
    }

    public function readSnapshot($modelName, $id) {
        if ($id === null) { return null; }
        $entity = $this->roEm->find($modelName, $id);
        if ($entity !== null) {
            $this->roEm->detach($entity);
        }
        return $entity;
    }

    /** Create a Doctrine Criteria Expression from a DAO search condition. */
    private function getCriteriaExpr($condition) {
        $field = $condition->getFieldName();
        $operator = $condition->getOperator();
        $value = $condition->getValue();
        switch ($operator) {
            case '=':
                if ($value === null) {
                    return Criteria::expr()->isNull($field);
                } else {
                    return Criteria::expr()->eq($field, $value);
                }
            case '!=': return Criteria::expr()->neq($field, $value);
            case '>': return Criteria::expr()->gt($field, $value);
            case '>=': return Criteria::expr()->gte($field, $value);
            case '<': return Criteria::expr()->lt($field, $value);
            case '<=': return Criteria::expr()->lte($field, $value);
            default: throw new \InvalidArgumentException(sprintf('Unsupported Search operator %s'), $$operator);
        }
    }

    /** Build a Criteria from a DAOCondition or an array of DAOConditions. */
    private function getCriteria($conditions = null, $count = null, $offset = null, $order = null) {
        // Convert single condition to array for mixed signature
        if ($conditions === null) {
            $conditions = array();
        } else if (!is_array($conditions)) {
            $conditions = array($conditions);
        }
        // Create filter by conditions
        $criteria = Criteria::create();
        if (count($conditions) > 0) {
            $criteria = $criteria->where($this->getCriteriaExpr($conditions[0]));
            for ($i = 1; $i < count($conditions); $i++) {
                $criteria = $criteria->andWhere($this->getCriteriaExpr($conditions[$i]));
            }
        }
        // Set order by
        if ($order != null) {
            $doctrineOrder = array();
            if (!is_array($order)) { $order = array($order); }
            foreach ($order as $ord) {
                switch (substr($ord, 0, 1)) {
                    case '+': $doctrineOrder[substr($ord, 1)] = Criteria::ASC; break;
                    case '-': $doctrineOrder[substr($ord, 1)] = Criteria::DESC; break;
                    default: $doctrineOrder[$ord] = Criteria::ASC; break;
                }
            }
            $criteria = $criteria->orderBy($doctrineOrder);
        }
        // Paging
        if ($count !== null) {
            $criteria = $criteria->setMaxResults($count);
        }
        if ($offset !== null) {
            $criteria = $criteria->setFirstResult($offset);
        }
        return $criteria;
    }

    public function search($modelName, $conditions = null,
            $count = null, $offset = null, $order = null) {

        $criteria = $this->getCriteria(null, $count, $offset, $order);
        return $this->em->getRepository($modelName)->matching($criteria)->getValues();
    }

    public function count($modelName, $conditions = array()) {
        $criteria = $this->getCriteria($conditions);
        return $this->em->getRepository($modelName)->matching($criteria)->count();
    }

    public function delete($model) {
        if ($model->getId() === null) { return; }
        if (!$this->inTransaction) { $this->startTransaction(); }
        $this->em->remove($model);
    }

    public function close() {
        $this->em->close();
        $this->roEm->close();
        $this->em->getConnection()->close();
        $this->roEm->getConnection()->close();
    }

    /** Access to low-level implementation.
     * Should be used only for unit testing purposes. */
    public function getEntityManager() {
        return $this->em;
    }
}
