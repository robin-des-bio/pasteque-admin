<?php
//    Pastèque API
//
//    Copyright (C) 2012-2015 Scil (http://scil.coop)
//    Cédric Houbart, Philippe Pary
//
//    This file is part of Pastèque.
//
//    Pastèque is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Pastèque is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Pastèque.  If not, see <http://www.gnu.org/licenses/>.

namespace Pasteque\Admin\System\DAO;

/** Parent class of all models to use with Doctrine. */
abstract class DoctrineModel
{
    /** List primitive typed field names of the model in an array, excluding id. */
    protected abstract function getDirectFieldNames();
    /** List reference field of the model in an array. A field is an associative
     * array with the following keys:
     * name: The field name (declared in code)
     * class: The full class name of the reference.
     * array (optional): can only be true if set, flag for XtoMany fields.
     * null (optional): can only be true if set, flag for nullable.
     * embedded (optional): can only be true if set, flag for subclasses.
     * Embedded values can be created on the fly from struct and are embedded
     * in toStruct. Non embedded fields are referenced only by id.
     * Embedded classes don't have their own id.
     * internal (optional): can only be true if set. Internal fields are not
     * read and exported in structs.
     * */
    protected abstract function getAssociationFields();

    protected function getAllowedFields() {
        return $this->getDirectFieldNames();
    }


    // Required for toStruct for references. */
    public function getId() { return $this->id; }

    protected function directFieldToStruct($field) {
        $value = call_user_func(array($this, 'get' . ucfirst($field)));
        switch (gettype($value)) {
            case 'resource':
                return base64_encode(stream_get_contents($value));
            default:
                return $value;
        }
    }

    protected function associationFieldToStruct($field) {
        $value = call_user_func(array($this, 'get' . ucfirst($field['name'])));
        // Association field
        if (!empty($field['array'])) {
            // Value is a ArrayCollection
            $struct = array();
            for ($i = 0; $i < $value->count(); $i++) {
                if (empty($field['embedded'])) {
                    $struct[] = $value->get($i)->getId();
                } else {
                    $struct[] = $value->get($i)->toStruct();
                }
            }
            return $struct;
        } else {
            if ($value === null) {
                return null;
            } else {
                if (empty($field['embedded'])) {
                    return $value->getId();
                } else {
                    return $value->toStruct();
                }
            }
        }
    }

    /** Unlink the model from DAO and all methods.
     * All references are converted to their Id.
     * @return An associative array with raw data, suitable for
     * json encoding. */
    public function toStruct() {
        // Get Doctrine fields and render them to delete the proxies
        $data = ['id' => $this->getId()];
        foreach ($this->getDirectFieldNames() as $field) {
            $data[$field] = $this->directFieldToStruct($field);
        }
        foreach ($this->getAssociationFields() as $field) {
            if (empty($field['internal'])) {
                $data[$field['name']] = $this->associationFieldToStruct($field);
            }
        }
        return $data;
    }

    /** Build an instance from struct. If id is set, it will ask the DAO to set
     * the current values. If not a new instance is created and returned.
     * @throws \UnexpectedValueException If id is set but no data is found. */
    protected static function instanceFromStruct($struct, $dao, $embedded = false) {
        $model = null;
        if (isset($struct['id'])) {
            $model = $dao->find(static::class, $struct['id']);
            if ($model === null && !$embedded) {
                // Embedded classes don't have their own id, so it's ok
                // not to find them: it's a new instance.
                throw now \UnexpectedValueException('No data found with this Id');
            }
        } else {
            $model = new static();
        }
        return $model;
    }
    /** Set the primitive typed fields from struct. */
    protected function directFieldsFromStruct($struct, $dao) {
        foreach ($this->getDirectFieldNames() as $field) {
            if ($field == 'id') { continue; } // Doctrine loads it
            if (array_key_exists($field, $struct)) {
                $value = $struct[$field];
                call_user_func(array($this, 'set' . ucfirst($field)), $struct[$field]);
            }
        }
    }

    protected static function readAssociationValue($struct, $dao, $field) {
        if (!empty($field['array'])) {
            $submodels = new \Doctrine\Common\Collections\ArrayCollection();
            foreach ($struct[$field['name']] as $content) {
                if (!empty($field['embedded'])) {
                    $fromStruct = new \ReflectionMethod($field['class'], 'fromStruct');
                    $submodel = $fromStruct->invoke(null, $content, $dao, true);
                } else {
                    $submodel = $dao->read($field['class'], $content['id']);
                    if ($submodel === null) {
                        throw new \UnexpectedValueException(sprintf('%s was not found', $field['name']));
                    }
                }
                $submodels->add($submodel);
            }
            return $submodels;
        } else {
            if (!empty($field['embedded'])) {
                $fromStruct = new \ReflectionMethod($field['class'], 'fromStruct');
                $submodel = $fromStruct->invoke(null, $struct[$field['name']], $dao, true);
            } else {
                $submodel = $dao->read($field['class'], $struct[$field['name']]);
                if ($submodel === null) {
                    throw new \UnexpectedValueException(sprintf('%s was not found', $field['name']));
                }
            }
            return $submodel;
        }
    }
    /** Build a model linked to the DAO from raw data.
     * @param $struct An associative array containing the data.
     * @param $dao The DAO to link to.
     * @param $embedded Only for recursive calls, don't set it.
     * @throw \UnexpectedValueException If Id is set but not data is found.
     * Or when an non nullable field is not set. */
    public static function fromStruct($struct, $dao, $embedded = false) {
        $model = static::instanceFromStruct($struct, $dao, $embedded);
        $model->directFieldsFromStruct($struct, $dao);
        $associationFields = static::getAssociationFields();
        foreach ($associationFields as $field) {
            if (!empty($field['internal'])) {
                // Internal fields are read from dao in instanceFromStruct
                continue;
            }
            $fieldName = $field['name'];
            // Check for required (not null)
            if (empty($field['null']) && empty($struct[$fieldName])) {
                if (empty($field['array'])) {
                    throw new \UnexpectedValueException(sprintf('%s is required', $fieldName));
                } else {
                    // Allow ommiting empty arrays as 'keep'
                }
            }
            // Get value and assign it.
            if (!empty($struct[$fieldName])) {
                $value = static::readAssociationValue($struct, $dao, $field);
                if (!empty($field['array'])) {
                    // At that point values are loaded from database by Doctrine
                    // Clear and rebuild
                    call_user_func(array($model, 'clear' . ucfirst($fieldName)));
                }
                call_user_func(array($model, 'set' . ucfirst($fieldName)), $value);
            }
        }
        return $model;
    }


    public function fromArray(array $userInput)
    {
        foreach ($userInput as $key => $value) {
            if (in_array($key, $this->getAllowedFields())) {
                $this->$key = $value;
            }
        }
    }

    public function toArray()
    {
        return get_object_vars($this);
    }



}
