<?php
//    Pastèque API
//
//    Copyright (C) 2017 Pastèque Contributors
//
//    This file is part of Pastèque.
//
//    Pastèque is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Pastèque is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Pastèque.  If not, see <http://www.gnu.org/licenses/>.

namespace Pasteque\Admin\Model;

use Doctrine\ORM\Mapping as ORM;
use \Pasteque\Admin\System\DAO\DoctrineModel;

/**
 * Class CashRegister
 * @package Pasteque
 * @SWG\Definition(type="object")
 * @Entity
 * @Table(name="cashregisters")
 */
class CashRegister extends DoctrineModel
{

    protected function getDirectFieldNames() {
        return ['id', 'reference', 'label'];
    }


    protected function getAssociationFields() {
        return [];
    }


    /**
     * ID of the cash register
     * @var integer
     * @SWG\Property()
     * @Id @Column(type="integer")
     * @GeneratedValue
     */
    protected $id;
    public function getId() { return $this->id; }

    /**
     * Code of the cash register, user-friendly ID.
     * It is automatically set from label if not explicitely set.
     * @var string
     * @SWG\Property()
     * @Column(type="string", unique=true)
     */
    protected $reference;
    public function getReference() { return $this->reference; }
    public function setReference($reference) { $this->reference = $reference; }

    /**
     * name of the cash register
     * @var string
     * @SWG\Property()
     * @Column(type="string")
     */
    protected $label;
    public function getLabel() { return $this->label; }
    public function setLabel($label) {
        $this->label = $label;
        if ($this->getReference() === null) {
            $this->setReference($label);
        }
    }






}
