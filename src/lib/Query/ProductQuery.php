<?php
//    Pastèque API
//
//    Copyright (C) 2012-2015 Scil (http://scil.coop)
//    Cédric Houbart, Philippe Pary
//
//    This file is part of Pastèque.
//
//    Pastèque is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Pastèque is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Pastèque.  If not, see <http://www.gnu.org/licenses/>.

namespace Pasteque\Admin\Query;


class ProductQuery
{

    public static $champs_recherche = ['label', 'reference', 'barcode'];


    public static function getAll($tab_id, $order = array(), $offset = 0, $limit = null)
    {

        return getAllObjet('product', $tab_id, $order, $offset, $limit);

    }


    public static function getWhere($params)
    {

        list($from, $where)=getWhereObjet('product',$params);

        if ($category = request_ou_options('category', $params)) {

            if (!is_array($category)){
                $category = [$category];
            }
            $where .= ' AND category_id IN('.implode(',',$category).')';
        }

        return [$from, $where];

    }

}
