<?php

namespace Pasteque\Admin\Forms;

use Symfony\Component\Form\AbstractType;


use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;


class PaymentModeForm extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $tab_floor = tab('paymentmode');
        $tab_order= ['In first'=>1] ;
        foreach($tab_floor as $floor){
            $tab_order[$floor['label']]=$floor['disp_order'];
        }
        $builder
            ->add('label', TextType::class, [])
            ->add('backlabel', TextType::class, [])
            ->add('disp_order', ChoiceType::class, ['choices'=>$tab_order])
            ->add('visible', CheckboxType::class, []);
               }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'name' => 'paymentmode',
        ]);
    }
}