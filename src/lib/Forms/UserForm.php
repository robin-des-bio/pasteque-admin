<?php

namespace Pasteque\Admin\Forms;

use Symfony\Component\Form\AbstractType;


use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;


class UserForm extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $tab_role =array_flip(table_simplifier(tab('role'),'name'));

        $builder->add('name', TextType::class, [ 'attr' => ['class' => '']])
            ->add('roles',  ChoiceType::class, ['choices' => $tab_role, 'expanded' => false, 'multiple' => false])
            ->add('active', CheckboxType::class, [])
            ->add('image', FileType::class, array('label' => 'Photo', 'attr' => array('class' => 'jq-ufs secondaire')));
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'name' => 'user',
        ]);
    }
}