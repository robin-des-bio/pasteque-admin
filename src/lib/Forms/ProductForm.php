<?php


namespace Pasteque\Admin\Forms;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;


class ProductForm extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $tab_categories = array_flip(table_simplifier(tab('category')));
        $tab_tax = array_flip(table_simplifier(tab('tax')));

        $builder
            ->add('category_id', ChoiceType::CLASS, array('label'=>'Category','choices' => $tab_categories, 'expanded' => false, 'multiple' => false, 'attr' => ['class' => '']))
            ->add('barcode', TextType::CLASS, array('attr' => ['class' => '']))
            ->add('reference', TextType::CLASS, array('attr' => ['class' => '']))
            ->add('label', TextType::CLASS, array('attr' => ['class' => '']))
            ->add('priceBuy', MoneyType::CLASS, array('attr' => ['class' => '']))
            ->add('priceSell', MoneyType::CLASS, array('attr' => ['class' => '']))
            ->add('priceSellvat', MoneyType::CLASS, array('attr' => ['class' => '']))
            ->add('realsell', HiddenType::CLASS, array('attr' => ['class' => '']))
            ->add('tax_id', ChoiceType::CLASS, array('choices' => $tab_tax, 'expanded' => false, 'multiple' => false, 'attr' => ['class' => '']))
            ->add('discountEnabled', CheckboxType::class, array(
                'required' => false,
                'attr' => array('align_with_widget' => true, 'class' => 'bs_switch')))
            ->add('discountRate', NumberType::CLASS, array('attr' => ['class' => '']))
        ->add('scaled', CheckboxType::class, array(
        'required' => false,
        'attr' => array('align_with_widget' => true, 'class' => 'bs_switch')));
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            //   'data_class' => 'zone',
            'name' => 'zone',
        ]);
    }
}