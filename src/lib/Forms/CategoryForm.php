<?php


namespace Pasteque\Admin\Forms;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;


class CategoryForm extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $tab_categories = ['']+array_flip(table_simplifier(tab('category')));
     
        $builder
            ->add('category_id', ChoiceType::CLASS, array('choices' => $tab_categories, 'expanded' => false, 'multiple' => false, 'attr' => ['class' => '']))
            ->add('reference', TextType::CLASS, array('attr' => ['class' => '']))
            ->add('label', TextType::CLASS, array('attr' => ['class' => '']))
            ->add('dispOrder', NumberType::CLASS, array('attr' => ['class' => '']));

    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([

            'name' => 'category',
        ]);
    }
}