<?php

namespace Pasteque\Admin\Forms;

use Symfony\Component\Form\AbstractType;


use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;


class CustomerForm extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder->add('firstname', TextType::class, array( 'attr' => array('class' => '')))
            ->add('firstname', TextType::class, array('label' => 'nom_famille','attr' => array('class' => '')))
            ->add('lastname', TextType::class, array( 'attr' => array('class' => '')))
            ->add('addr1', TextType::class, array('label' => 'address'))
            ->add('addr2', TextType::class, array('label' => 'address'))
            ->add('zipcode', TextType::class, array('label' => 'zip code'))
            ->add('city', TextType::class, array())
            ->add('region', TextType::class, array())
            ->add('country', ChoiceType::class, array('choices' => array_flip(tab('pays'))))
            ->add('phone1', TextType::class, array( 'attr' => array('class' => 'phone')))
            ->add('phone2', TextType::class, array('attr' => array('class' => 'phone')))
            ->add('fax', TextType::class, array( 'attr' => array('class' => 'phone')))
            ->add('email', EmailType::class, array('attr' => array('placeholder' => 'adresse@email.fr') ))
            ->add('note', TextareaType::class, array(  'attr'=>['class'=>'secondaire']))
            ->add('image', FileType::class, array('label' => 'Photo', 'attr' => array('class' => 'jq-ufs secondaire')));
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'name' => 'customer',
        ]);
    }
}