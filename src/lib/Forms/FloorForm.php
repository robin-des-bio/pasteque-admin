<?php


namespace Pasteque\Admin\Forms;

use Symfony\Component\Form\AbstractType;


use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;


class FloorForm extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $tab_floor = tab('floor');
        $tab_order= ['In first'=>1] ;
        foreach($tab_floor as $floor){
            $tab_order[$floor['label']]=$floor['disp_order'];
        }
        $builder
            ->add('label', TextType::CLASS, array('attr' => ['class' => '']))
            ->add('image', FileType::class, array( 'attr' => array('class' => 'jq-ufs secondaire')))
            ->add('disp_order', ChoiceType::class, ['choices'=>$tab_order]);

    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'name' => 'place',
        ]);
    }
}