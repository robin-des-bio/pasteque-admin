<?php

namespace Pasteque\Admin\Forms;

use Symfony\Component\Form\AbstractType;


use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;


class CurrencyForm extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder
            ->add('reference', TextType::class, [])
            ->add('label', TextType::class, [])
            ->add('symbol', TextType::class, [])
            ->add('decimalSeparator', TextType::class, [])
            ->add('thousandsSeparator', TextType::class, [])
            ->add('format', TextType::class, [])
            ->add('rate', NumberType::class, [])
            ->add('main', CheckboxType::class, [])
            ->add('visible', CheckboxType::class,[]);
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'name' => 'currency',
        ]);
    }
}