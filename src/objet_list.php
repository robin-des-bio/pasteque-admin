<?php
function objet_list()
{
    global $app;
    $args_twig=[
        'tab_col'=>objet_colonnes()
    ];
    return $app['twig']->render(fichier_twig(), $args_twig);

}


function objet_colonnes()
{

    $objet = sac('objet');
    $tab_champs = descr($objet . '.colonnes');
    $tab_colonne = array();
    foreach ($tab_champs as $k =>$v) {
        $tab_colonne[$k] = ['title' => $k];
    }
    $tab_colonne['action']=["orderable"=> false];
    $tab_colonne = datatable_complete_liste_colonne($tab_colonne);

    return $tab_colonne;

}
