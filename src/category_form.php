<?php


use Symfony\Component\Form\FormError as FormError;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Pasteque\Admin\Model\Category;
use Pasteque\Admin\Forms\CategoryForm;


function category_form()
{
    global $app;
    $args_rep = [];
    $request = $app['request'];
    $id = sac('id');
    if ($id) {
        $modification = true;
        $category = charger_objet();
        $data = $category->toArray();

    } else {
        $modification = false;
        $data = ['rate'=>0];

    }
    $builder = $app['form.factory']->createNamedBuilder('category',CategoryForm::class,$data);
    $builder->setRequired(false);
    formSetAction($builder,$modification,['id'=>$id]);

    $form = $builder->add('submit',SubmitType::class,
                array('label' => $app->trans('Save'), 'attr' => array('class' => 'btn-primary')))
            ->getForm();


    $form->handleRequest($request);
    if ($form->isSubmitted()) {
        $data_form = $form->getData();
        if ($form->isValid()) {

            if (!$modification){
                $category = new category();
            }
            else
            {
                $reference_origine = $category->getReference();
            }

            $category->fromArray($data_form);

            if ($modification){
                list($reponse, $statut, $err) = appelAPI('/api/category',[],$category->toStruct(),[],'POST');
            }
            else{
                $cat = $category->toStruct();
                // TODO : Passer en méthode PATCH
                //unset($pr['id']);
                //list($reponse, $statut, $err) = appelAPI('/api/category/'.$reference_origine,[],$pr,[],'PATCH');
                list($reponse, $statut, $err) = appelAPI('/api/category',[],$category->toStruct(),[],'POST');
            }
            if ($statut==200){
                $app['orm.em']->persist($category);
                $app['orm.em']->flush();
            }



        }
    }
    $args_rep['js_init'] = 'category_form';
    return reponse_formulaire($form,$args_rep);

}


function action_category_form_supprimer(){
    return action_supprimer_une_instance();
}
