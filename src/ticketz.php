<?php

use \Pasteque\Admin\Query\CashSessionQuery;

function ticketz()
{
    global $app;
    $request = $app['request'];

    list($reponse, $statut, $err) = appelAPI('/api/cash/search/', ['dateStart' => '1981-01-16']);
    $reponse = json_decode(json_encode($reponse), true);

    $format = $request->get('format');
    $table = descr('session.table_sql');
    $primary_key = descr('session.cle_sql');
    $colonnes = descr('session.colonnes');
    $tab_temp = $app['db']->fetchAll('SELECT ' . $primary_key . ' FROM ' . $table);
    $tab = [$primary_key => []];
    foreach ($tab_temp as $temp) {
        $tab[$primary_key][] = $temp[$primary_key];
    }


    foreach ($reponse as $values) {
        if (!in_array($values[$primary_key], $tab[$primary_key])) {

            $values['cashregister_id'] = $values['cashRegister'];
            $values_session = array_intersect_key($values, $colonnes);
            $values_session['openDate'] = new DateTime("@" . $values_session['openDate']);
            $values_session['openDate'] = $values_session['openDate']->format('Y-m-d H:i:s');
            $values_session['closeDate'] = new DateTime("@" . $values_session['closeDate']);
            $values_session['closeDate'] = $values_session['closeDate']->format('Y-m-d H:i:s');
            $app['db']->insert($table, $values_session);
            $id_session = $app['db']->lastInsertId();
            import_data('sessionpayment', $values['payments'], $id_session, ['paymentmode' => 'paymentMode', 'currency' => 'currency']);
            import_data('sessiontaxe', $values['taxes'], $id_session, ['tax' => 'tax']);
            import_data('sessioncat', $values['catSales'], $id_session, []);
            import_data('sessioncattaxe', $values['catTaxes'], $id_session, ['tax' => 'tax']);
            import_data('sessioncustbalance', $values['custBalances'], $id_session, ['customer' => 'customer']);

        }
    }


    $datefilter = $request->get('datefilter');
    if (!empty($datefilter)) {

        $dates = explode(' - ', $datefilter);
        $date_start = \DateTime::createFromFormat("d/m/Y H:i:s", $dates[0] . ' 00:00:00');
        $date_stop = \DateTime::createFromFormat("d/m/Y H:i:s", $dates[1] . ' 23:59:59');


    } else {
        $date_start = $date_stop = new \DateTime();
        $date_start = \DateTime::createFromFormat("d/m/Y H:i:s", $date_start->format('d/m/Y') . ' 00:00:00');
        $date_stop = \DateTime::createFromFormat("d/m/Y H:i:s", $date_start->format('d/m/Y') . ' 23:59:59');

    }
    $params = ['date_start' => $date_start, 'date_stop' => $date_stop];


    $tab_dates = array();
    $tab_dates["selection"] = array($date_start->format('d/m/Y'), $date_stop->format('d/m/Y'));
    $date = new \DateTime();
    $tab_dates["aujourdhui"] = array($date->format('d/m/Y'), $date->format('d/m/Y'));
    $date = $date->sub(new \DateInterval('P1D'));
    $tab_dates["hier"] = array($date->format('d/m/Y'), $date->format('d/m/Y'));
    $date1 = new \DateTime();
    $date1->sub(new \DateInterval('P7D'));
    $date2 = new \DateTime();
    $tab_dates["7jours"] = array($date1->format('d/m/Y'), $date2->format('d/m/Y'));
    $date1 = new \DateTime();
    $date1->sub(new \DateInterval('P30D'));
    $tab_dates["30jours"] = array($date1->format('d/m/Y'), $date2->format('d/m/Y'));
    $date1 = \DateTime::createFromFormat("d/m/Y", '01/' . date('m') . '/' . date('Y'));
    $date2 = \DateTime::createFromFormat("d/m/Y", '01/' . date('m') . '/' . date('Y'))->add(new \DateInterval('P1M'))->sub(new \DateInterval('P1D'));
    $tab_dates["mois_en_cours"] = array($date1->format('d/m/Y'), $date2->format('d/m/Y'));
    $date1 = \DateTime::createFromFormat("d/m/Y", '01/' . date('m') . '/' . date('Y'))->sub(new \DateInterval('P1M'));
    $date2 = \DateTime::createFromFormat("d/m/Y", '01/' . date('m') . '/' . date('Y'))->sub(new \DateInterval('P1D'));
    $tab_dates["mois_dernier"] = array($date1->format('d/m/Y'), $date2->format('d/m/Y'));
    $tab_dates["date_start"] = $date_start->format('d/m/Y');
    $tab_dates["date_stop"] = $date_stop->format('d/m/Y');

    list($tab_session, $nb) = objet_liste_dataliste_preselection('session', $params, false, false);


    if ($format == 'csv') {


        header("Content-Type: text/csv");
        header("Content-Disposition: attachment; filename=ticketz.csv");

        $tab_colonne = ['caisse', 'session', 'open date', 'close date', 'close cash', 'close cash', 'Expected cash', 'Ticket count', 'Amount'];
        $tab_tax = tab('tax');
        foreach ($tab_tax as $tax) {
            $tab_colonne[] = 'HT ' . $tax['rate'];
            $tab_colonne[] = $tax['label'];
        }
        $tab_paymentmode = tab('paymentmode');
        foreach ($tab_paymentmode as $payment) {
            $tab_colonne[] = $payment['label'];
        }

        echo (implode("\t", $tab_colonne)) . PHP_EOL;

        $tab_session = CashSessionQuery::getAll($tab_session);
        foreach ($tab_session as $s) {
            $tab = [
                $s->getCashregister()->getId(),
                $s->getId(),
                $s->getOpenDate()->format('d/m/Y'),
                $s->getCloseDate()->format('d/m/Y'),
                $s->getOpenCash(),
                $s->getCloseCash(),
                $s->getExpectedCash(),
                $s->getTicketCount(),
                $s->getCs()
            ];

            $tab_taxes_q = $s->getTaxes();
            $tab_taxes = [];
            foreach ($tab_taxes_q as $t) {
                $tab_taxes[$t->getId()['tax']] = $t;
            }

            foreach ($tab_tax as $tax) {
                $tab[] = isset($tab_taxes[$tax['id']]) ? $tab_taxes[$tax['id']]->getBase() : 0;
                $tab[] = isset($tab_taxes[$tax['id']]) ? $tab_taxes[$tax['id']]->getAmount() : 0;
            }

            $tab_payments_q = $s->getPayments();
            $tab_payments = [];
            foreach ($tab_payments_q as $p) {
                $tab_payments[$p->getId()['paymentMode']] = $p;
            }

            foreach ($tab_paymentmode as $payment) {
                $tab[] = isset($tab_payments[$payment['id']]) ? $tab_payments[$payment['id']]->getAmount() : 0;
            }

            echo (implode("\t", $tab)) . PHP_EOL;

        }

        exit();
    }


    $args_twig = [
        'tab_cashregister' => tab('cashregister'),
        'tab_paymentmode' => tab('paymentmode'),
        'tab_tax' => tab('tax'),
        'tab_session' => CashSessionQuery::getAll($tab_session),
        'tab_dates' => $tab_dates
    ];
    return $app['twig']->render(fichier_twig(), $args_twig);
}


function import_data($objet, $data, $id_session, $tab_key)
{
    global $app;
    $table = descr($objet . '.table_sql');
    $colonnes = descr($objet . '.colonnes');
    $app['db']->delete($table, ['cashsession_id' => $id_session]);

    foreach ($data as $values) {

        $values['cashsession_id'] = $id_session;
        foreach ($tab_key as $k => $key)
            $values[$k . '_id'] = $values['id'][$key];
        $values_session = array_intersect_key($values, $colonnes);

        $app['db']->insert($table, $values_session);

    }

}