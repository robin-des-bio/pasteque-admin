<?php


use Symfony\Component\Form\FormError as FormError;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Pasteque\Admin\Model\PaymentMode;
use Pasteque\Admin\Forms\PaymentModeForm;


function paymentmode_form()
{
    global $app;
    $args_rep = [];
    $request = $app['request'];
    $id = sac('id');
    if ($id) {
        $modification = true;
        $paymentmode = charger_objet();
        $data = $paymentmode->toArray();

    } else {
        $modification = false;
        $data = ['rate'=>0];

    }
    $builder = $app['form.factory']->createNamedBuilder('paymentmode',PaymentModeForm::class,$data);
    $builder->setRequired(false);
    formSetAction($builder,$modification,['id'=>$id]);

    $form = $builder->add('submit',SubmitType::class,
                array('label' => $app->trans('Save'), 'attr' => array('class' => 'btn-primary')))
            ->getForm();


    $form->handleRequest($request);
    if ($form->isSubmitted()) {
        $data_form = $form->getData();
        if ($form->isValid()) {

            if (!$modification){
                $paymentmode = new paymentmode();
            }
            else
            {
                $reference_origine = $paymentmode->getReference();
            }

            $paymentmode->fromArray($data_form);

            if ($modification){
                list($reponse, $statut, $err) = appelAPI('/api/paymentmode',[],$paymentmode->toStruct(),[],'POST');
            }
            else{
                //Création du mode paiement


               }
            if ($statut==200){
                $app['orm.em']->persist($paymentmode);
                $app['orm.em']->flush();
            }



        }
    }
    return reponse_formulaire($form,$args_rep);

}


function action_paymentmode_form_supprimer(){
    return action_supprimer_une_instance();
}
