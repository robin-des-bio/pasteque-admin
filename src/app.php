<?php


use Silex\Provider\FormServiceProvider;
use Silex\Provider\HttpCacheServiceProvider;
use Silex\Provider\MonologServiceProvider;
use Silex\Provider\SecurityServiceProvider;
use Silex\Provider\SessionServiceProvider;
use Silex\Provider\TranslationServiceProvider;
use Silex\Provider\TwigServiceProvider;
use Silex\Provider\ValidatorServiceProvider;
use Symfony\Component\Security\Core\Encoder\PasswordEncoder;
use Symfony\Component\Security\Core\Encoder\PlaintextPasswordEncoder;
use Symfony\Component\Translation\Loader\YamlFileLoader;
use Symfony\Component\Validator\Constraints as Assert;
use WhoopsSilex\WhoopsServiceProvider;
use Silex\Provider\DoctrineServiceProvider;
use Dflydev\Provider\DoctrineOrm\DoctrineOrmServiceProvider;
use Doctrine\Common\Cache\ApcuCache;


if ($app['debug']) {
    $app->register(new WhoopsServiceProvider);

}
$app->register(new HttpCacheServiceProvider());
$app->register(new SessionServiceProvider(), [
    'session.storage.options' => [
        'name' => $app['session.variable.name'],
        'cookie_lifetime' => $app['session.variable.lifetime'] = 3600
    ]
]);
$app->register(new ValidatorServiceProvider());
$app->register(new FormServiceProvider());
$app->register(new Moust\Silex\Provider\CacheServiceProvider(), array(
    'cache.options' => $app['cache.driver']
));




$app->register(new SecurityServiceProvider());

$app->register(new Silex\Provider\RememberMeServiceProvider());
$app['users'] = function () use ($app) {
    return new UserProvider();
};


$app['security.firewalls'] = array(

    'login' => array(
        'pattern' => '/(login$|oauth)',
        'anonymous' => true,
        'switch_user' => false
    ),
    'default' => array(
        'pattern' => '^.*$',
        'form' => array(
            'login_path' => '/login',
            'username_parameter' => 'login[username]',
            'password_parameter' => 'login[password]'
        ),
        'logout' => true,
        'anonymous' => false,
        'users' => $app['users'],
        'switch_user' => false,
        'remember_me' => array(
            'key' => 'pasteque_cookie',
            'always_remember_me' => true
        ),
    ),

);
$app['security.default_encoder'] = function () use ($app) {
    return new PlaintextPasswordEncoder();
};



ini_set('date.timezone', $app['date.timezone']);
date_default_timezone_set($app['date.timezone']);
$app->register(new Silex\Provider\LocaleServiceProvider());
$app->register(new TranslationServiceProvider(), array(
    'locale_fallbacks' => array('fr'),
));

$app->extend('translator', function ($translator, $app) {
    $translator->addLoader('yaml', new YamlFileLoader());
    foreach ($app['translator.messages'] as $locale => $files) {
        foreach ($files as $file) {
            $translator->addResource('yaml', $file, $locale);
        }
    }
    return $translator;
});


$app->register(new MonologServiceProvider(), array(
    'monolog.logfile' => $app['log.path'] . '/app.log',
    'monolog.name' => 'app',
    'monolog.level' => 300 // = Logger::WARNING
));

$app->register(new TwigServiceProvider(), array(
    'twig.options' => array(
        'cache' => isset($app['twig.options.cache']) ? $app['twig.options.cache'] : false,
        'strict_variables' => true
    ),
    'twig.path' => array($app['resources.path'] . '/views')
));



$app->register(new Silex\Provider\SwiftmailerServiceProvider(), array(
    'swiftmailer.options' => $app['swiftmailer.options'],
    'swiftmailer.use_spool' => false

));

$app->register(new Silex\Provider\AssetServiceProvider(), array(
    'assets.version' => 'v1',
    'assets.version_format' => '%s?version=%s',
));

$app->register(new DoctrineServiceProvider(), [
    'db.options' => array(
        'driver' => 'pdo_sqlite',
        'path' => $app['tmp.path'].'/sqlite.db',
    )
]);



$app->register(new DoctrineOrmServiceProvider(), [
    'orm.proxies_dir' => $app['tmp.path'].'/cache_data',
    'orm.em.options'=>[
        'mappings' => [
            [
                'type' => 'annotation',
                'namespace' => 'Pasteque\Admin',
                'path' => $app['basepath'].'/src/lib/Model',
            ],
        ],
    ]

]);





$app['request'] = function () use ($app) {
    return $app['request_stack']->getCurrentRequest();
};

require_once($app['basepath'] . '/src/inc/fonctions_twig_basic.php');



return $app;
