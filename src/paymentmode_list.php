<?php

function paymentmode_list()
{
    global $app;
    $args_twig = [
        'tab_col' => paymentmode_colonnes(),
        'tab_filtres' => paymentmode_filtres()
    ];
    return $app['twig']->render(fichier_twig(), $args_twig);
}


function paymentmode_colonnes()
{

    $tab_colonne = array();
    $tab_colonne['id'] = ['title' => 'id'];
    $tab_colonne['reference'] = [];
    $tab_colonne['label'] = [];
    $tab_colonne['backLabel'] = [];
    $tab_colonne['action'] = ["orderable" => false];
    $tab_colonne = datatable_complete_liste_colonne($tab_colonne);
    return $tab_colonne;
}


function paymentmode_filtres()
{
    global $app;
    $request = $app['request'];
    $rechercher = $request->get('search');
    $tab_filtres['premier']['search'] = filtre_ajouter_recherche($rechercher['value']);
    return $tab_filtres;
}


function action_paymentmode_list_dataliste()
{
    return objet_liste_dataliste('paymentmode');
}


function action_paymentmode_list_rechercher()
{
    return objet_liste_rechercher('paymentmode');
}

