<?php

function cashregister_list()
{
    global $app;
    $args_twig = [
        'tab_col' => cashregister_colonnes(),
        'tab_filtres' => cashregister_filtres()
    ];
    return $app['twig']->render(fichier_twig(), $args_twig);
}


function cashregister_colonnes()
{

    $tab_colonne = array();
    $tab_colonne['id'] = ['title' => 'id'];
    $tab_colonne['reference'] = [];
    $tab_colonne['label'] = [];
    $tab_colonne['action'] = ["orderable" => false];
    $tab_colonne = datatable_complete_liste_colonne($tab_colonne);
    return $tab_colonne;
}


function cashregister_filtres()
{
    global $app;
    $request = $app['request'];
    $rechercher = $request->get('search');
    $tab_filtres['premier']['search'] = filtre_ajouter_recherche($rechercher['value']);
    return $tab_filtres;
}


function action_cashregister_list_dataliste()
{
    return objet_liste_dataliste('cashregister');
}


function action_cashregister_list_rechercher()
{
    return objet_liste_rechercher('cashregister');
}

