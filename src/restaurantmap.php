<?php

use \Pasteque\Admin\Model\Floor;
use \Pasteque\Admin\Model\Place;


function restaurantmap()
{
    global $app;
    $args_twig = [
        'tab_floor' => tab('floor'),
        'tab_place' => tab('place')
    ];
    return $app['twig']->render(fichier_twig(), $args_twig);
}

