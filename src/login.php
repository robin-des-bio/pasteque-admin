<?php


use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

function login($request){

    global $app;

    $form = $app['form.factory']->createNamedBuilder('login',FormType::class)
        ->add('username',TextType::class,
            array('label' => 'Login', 'data' => $app['session']->get('_security.last_username')))
        ->add('password', PasswordType::class, array('label' => 'Mot de passe'))
        ->getForm();

    return $app['twig']->render('login.html.twig', array(
        'form' => $form->createView(),
        'error' => $app['security.last_error']($request),
    ));

}


function action_login_init_password($request){

    global $app;
    $error = null;
    $tokenExpired = false;
    $email= $request->get('email');
    $token= $request->get('token');
    $individu = IndividusQuery::create()->filterByEmail($email)->findOneByToken($token);
    if (!$individu) {
        $tokenExpired = true;
    } else if ($individu->isPasswordResetRequestExpired(3600)) {
        $tokenExpired = true;
    }
    if ($tokenExpired) {
        $app['session']->getFlashBag()->set('alert', 'Sorry, your password reset link has expired.');
        return $app->redirect($app['url_generator']->generate('login'));
    }
    $error = '';
    $data=array();
    $builder = $app['form.factory']->createNamedBuilder('individu_form_mdp',FormType::class, $data);
    $builder->setRequired(false);
    //formSetAction($builder,true,array('action'=>'changer_mdp'));
    $form = $builder->add('password', RepeatedType::class, array(
        'type' => PasswordType::class,
        'invalid_message' => 'The password fields must match.',
        'options' => array('attr' => array('class' => 'password-field','autocomplete' => 'off')),
        'required' => true,
        'first_options'  => array('label' => 'Mot de passe'),
        'second_options' => array('label' => 'Vérification mot de passe'),
    ))
        ->add('submit',SubmitType::class,   array('label' => $app->trans('Enregistrer'), 'attr' => array('class' => 'btn-primary')))
        ->getForm();

    $form->handleRequest($request);


    if ($form->isSubmitted()) {
        $data = $form->getData();

        if ($form->isValid()) {

            $individu->setPass($data['password']);
            $individu->setToken(null);

            $individu->save();
            $app['session']->getFlashBag()->set('alert', 'Your password has been reset and you are now signed in.');
            return $app->redirect($app->path('homepage'));

        } else {
            $form->addError(new FormError($app->trans('erreur_saisie_formulaire')));
        }
    }




    return $app['twig']->render('login_init_password.html.twig', array(
        'user' => $individu,
        'form' => $form->createView(),
        'error' => $error
    ));





}


function action_login_motdepasseperdu($request){

    global $app;
    $error = null;

    if ($request->isMethod('POST')) {
        $email = $request->request->get('email');
        $user = IndividusQuery::create()->findOneByEmail($email);
        if ($user) {
            // Initialize and send the password reset request.
            $user->setTokenTime(time());
            if (!$user->getToken()) {
                $user->setToken(uniqid());
            }
            $user->save();


            $args_twig = array(
                'url_reset'=>$app->url('login', array('action'=>'init_password','email'=>$email,'token' => $user->getToken()))
            );

            courriel_twig($email,'login_motdepasseperdu',$args_twig);

            $app['session']->getFlashBag()->set('alert', 'Instructions for resetting your password have been emailed to you.');
            $app['session']->set('_security.last_username', $email);

            return $app->redirect($app['url_generator']->generate('login'));
        }
        $error = 'Aucun utilisateur ne corresponds à cette adresse';

    } else {
        $email = $request->request->get('email') ?: ($request->query->get('email') ?: $app['session']->get('_security.last_username'));
        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) $email = '';
    }

    return $app['twig']->render('login_motdepasseperdu.html.twig', array(
        'email' => $email,
        'fromAddress' => $app['email_from'],
        'error' => $error,
    ));
}


