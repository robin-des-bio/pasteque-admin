<?php
// reste un PB d'arrondi dans le coordonées
// enregistré  3.1173578642614                         614
// openstreetmap renvoi 3.11735786426135               6135
// fait un round en attendant l'écriture correcte dans le fichier 
function donne_coordonnee_avec_adresse($adresse, $ville)
{
    $tab = false;
    $fp = fopen("http://nominatim.openstreetmap.org/?q=" . urlencode($adresse .', '. $ville) . "&format=json", "r");
    $chaine = "";
    while (($line = fgets($fp)) !== false) {
        $chaine .= $line;
    }
    $chaine = json_decode($chaine);
    if (isset($chaine[0])) {
        $tab = array('lat' => $chaine[0]->lat, 'lon' => $chaine[0]->lon, 'ad' => '');
    }
    if (!$tab) {
             fclose($fp);
            $chaine = "";
            $fp = fopen("http://nominatim.openstreetmap.org/?q=" . urlencode($ville) . "&format=json", "r");
            while (($line = fgets($fp)) !== false) {
                $chaine .= $line;
            }
            $chaine = json_decode($chaine);
            if (isset($chaine[0])) {
                $tab = array('lat' => $chaine[0]->lat, 'lon' => $chaine[0]->lon, 'ad' => '');
            }
    }
    fclose($fp);
    return $tab;

}


function formatAdressePourRecherche($adresse)
{
    $adresse = str_replace("/", "", $adresse);
    $adresse = str_replace("\r\n", " ", $adresse);
    $adresse = trim(preg_replace('`(Boulevard|bat|batiment|appartement|apt|app|appt|appart|etage|étage|etg) [0-9]{1,}(er|ere)?`i',
        '', $adresse));
    $adresse = trim(preg_replace('`([0-9]{1,}) (bd|bvd|bvld|bvlrd) `i', '%1 boulevard', $adresse));
    return $adresse;
}
function formatvillePourRecherche($ville)
{
    $res = str_replace("/", "", $ville);
    $res = str_replace("\r\n", " ", $res);
    $res = str_replace("CEDEX" ,"", $res);
    return $res;
}

function traitement_form_geo($objet_data, $data, $modification, $objet)
{

    $id = $objet_data->getPrimaryKey();
    $msg='';
    if (!empty($data['adresse']) or !empty($data['ville'])) {
        $adres = formatAdressePourRecherche($data['adresse']);
        $ville = formatVillePourRecherche($data['ville']);
        $coord = donne_coordonnee_avec_adresse($adres, $ville);
        $msg = $id . " - ".$data['nom'] ;
        $msg.= "\n       -adresse  : " . $adres ;
        $msg.= "\n       -ville  : " . $ville ;

        if ($coord) {
            $msg .= creer_gis_liens($coord['lon'], $coord['lat'], $objet, $id, $modification);
        }
        $msg .= PHP_EOL;
    }
    return $msg;
}


function creer_gis_liens($lon, $lat, $objet, $id, $modification)
{
//    echo '<br>lon :' . $lon . ' lat :' . $lat . ' objet :' . $objet . ' id :' . $id . ' modification :' . $modification;
    $lien = false;
    $id_gis_ancien = 0;
    $msg = ': lon :' . $lon . ' lat :' . $lat . ' objet :' . $objet . ' id :' . $id . '';
    $autre_objet = array();
    $id_gis = creer_corriger_les_points_en_double($lon, $lat);
// a revoir de 83 a 150
    if ($modification) {
        $tab_objet_gis_lien_ancien = GisliensQuery::create()->filterByObjet($objet)->filterByidObjet($id)->find();
        if (count($tab_objet_gis_lien_ancien) > 1) {// plusieurs lienss
            // existe t'il d'autres objets reliés a ces points
//            arbre($tab_objet_gis_lien_ancien);
            foreach ($tab_objet_gis_lien_ancien as $templien) {
                $id_gis_ancien = $templien->getidGis();
                $tab_objet_gis_ancien = GisQuery::create()->filterByidGis($id_gis_ancien)->find();
                if (isset($tab_objet_gis_ancien)) {
                    foreach ($tab_objet_gis_ancien as $tempgis) {//les points de chaque liens anciens
                        $tab_gis_liens_ancien = GisliensQuery::create()->filterByidGis($id_gis_ancien)->find();
                        if (isset($tab_gis_liens_ancien)) {
                            foreach ($tab_gis_liens_ancien as $temlien) {//les liens du point de la boucle de chaque liens anciens
                                if ($temlien->getObjet() != $objet or $templien->getidObjet() != $id) {
                                    if (isset($autre_lien[$id_gis_ancien])) {
                                        $autre_objet[$id_gis_ancien]++;
                                    } else {
                                        $autre_objet[$id_gis_ancien] = 1;
                                    }
                                }
                            }

                        }
                    }
                }
            }
            if (count($autre_objet)) {
                Echo 'nombre de liens externes';
                arbre($autre_objet);
            }
        }
        if (isset($objet_gis_liens)) {
            $id_gis_ancien = $objet_gis_liens->getidGis();
            $ancien = GisQuery::create()->filterByidGis($id_gis_ancien)->findone();
            $id_gis_ancien = creer_corriger_les_points_en_double($ancien->getLon(), $ancien->getLat());
            if ($id_gis_ancien) {
                $tab_gis_liens_ancien = GisliensQuery::create()->filterByidGis($id_gis_ancien)->find();
                $nb_lien_ancien_tous = count($tab_gis_liens_ancien);
                switch ($nb_lien_ancien_tous) {
                    case 0 :
//                        echo '<br>creation du lien';
                        $lien = true;
//                        ne devrait jamais passer ici
                        break;
                    case 1 :
                        echo '<br> ce point est il le bon ? lon' ;
//                        if ($objet_gis_ancien->getLat() == $lat and $objet_gis_ancien->getLon() == $lon) {
//                            echo '<br> le lien ancien indique la bon point pas de modif';
//                        } else {
//                            arbre($tab_gis_liens_ancien);
//                        }
                        echo "<br>le lien ancien n'indique pas le bon point on le modifie";
                        break;
                    default :
                        echo $nb_lien_ancien_tous . ' liens sur ' . $id_gis_ancien . ' creer un nouveau lien';
                        if (GisliensQuery::create()->filterByidGis($id_gis_ancien)->filterByObjet($objet)->filterByidObjet($id)->findone()) {
                            echo '<br>le lien sur ' . $id_gis_ancien . ' de id :' . $id . ' existe le modifier';
                        } else {
                            $lien = true;
                        }
                }
            }
        } else {//  echo '<br> Pas de lien ancien';
            $lien = true;
        }
    } else {
        $lien = true;
    }
    if ($lien) {
        $objet_gis_liens=GisliensQuery::create()->filterByidObjet($id)->findOneByObjet($objet);
        if (!$objet_gis_liens) {
            $objet_gis_liens = new GisLiens();
            $msg .= ' nouveau lien';
        }
    }
    $objet_gis_liens->setidGis($id_gis);
    $objet_gis_liens->setidObjet($id);
    $objet_gis_liens->setObjet($objet);
    $objet_gis_liens->save();
    return $msg;
}

function creer_corriger_les_points_en_double($lon, $lat)
{
//    echo '\r longitude'. $lon.' latitude '.$lat;
//    echo '\r longitude'. round($lon,13).' latitude '.round($lat,13);


    $tab_objet_gis = GisQuery::create()->filterByLon(round($lon,13))->filterByLat(round($lat,13))->find();
//    arbre($tab_objet_gis);

    if (count($tab_objet_gis)) {
//        echo '<br>nb :'.count($tab_objet_gis);
        $i = 0;
        foreach ($tab_objet_gis as $gis) {
            $i++;
            if ($i == 1) {
                $id_gis = $gis->getidGis();
//                echo '<br>ligne 190 '.$lon.'  '.$lat.' '.$id_gis;
            }
            //                echo "<br>Réafecter les liens du point en double sur le 1° point" . $lon . '  ' . $lat . ' ' . $id_gis;
                $tab_objet_gis_lien_ancien = GisliensQuery::create()->filterByidGis($gis->getidGis())->find();
                foreach ($tab_objet_gis_lien_ancien as $temp) {
//                    arbre($temp);
                    $temp->setidGis($id_gis);
                    $temp->save();
//                    echo "<br>lien modifié <br>";
//                    arbre($temp);
                }

            if ($i != 1) {
                    $id_gis = $gis->getidGis();
//                    echo "<br>point a effacer <br>";
//                arbre($gis);
                $gis->delete();
            }
        }
     } else {
//        echo 'ligne 200';
        $objet_gis_nouveau = new gis();
        $objet_gis_nouveau->setLat($lat);
        $objet_gis_nouveau->setLon($lon);
        $objet_gis_nouveau->save();
        $id_gis = $objet_gis_nouveau->getidGis();

    }
    return $id_gis;
}



function rechercherObjetsParZone($id_zone,$objet){

    $zone = ZonesQuery::create()->findPk($id_zone);
    $tab_gisl = GisliensQuery::create()->filterByObjet($objet)->useGisQuery()->endUse()->find();

    $tab_id_objet=[];
    if (!empty($tab_gisl)) {
        $tab_gis=[];
        foreach ($tab_gisl as $gl) {
            $g = $gl->getGis();
            $tab_gis[$g->getLon().' '.$g->getLat()][] = $gl->getidObjet();
        }

        $trace_geo = geoPHP::load('MULTIPOINT(' . implode(',',array_keys($tab_gis)) . ')', 'wkt');
        $trace_geo = $trace_geo->out('json');
        $trace_geo = geoPHP::load($trace_geo, 'json');
        $zone_geo = $zone->getZoneGeo();
        $tab_geos = json_decode($zone_geo);
        foreach ($tab_geos as $geo) {
            $geometry = geoPHP::load($geo->geojson, 'json');
            if ($geo_result = $trace_geo->intersection($geometry)) {
                $tab_point=array();
                    if(get_class($geo_result)=='Point'){
                        $tab_point[$geo_result->coords[0] . ' ' . $geo_result->coords[1]] = 1;
                    }
                else {
                    foreach ($geo_result->components as $k => $p) {
                        $tab_point[$p->coords[0] . ' ' . $p->coords[1]] = $k;
                    }
                }
                foreach($tab_point as $i=>$k){
                    $tab_id_objet = array_merge($tab_id_objet,array_values($tab_gis[$i]));
                }


            }


        }

    }
    return array_values($tab_id_objet);
}
