<?php


function variables_config()
{

    init_sac();

    return array(

        'bdd_version' => array(
            'variables' => bdd_version(),
            'systeme' => true
        ),
        'config_version' => array(
            'variables' => config_version(),
            'systeme' => true
        ),
        'pays' => array(
            'variables' => 'FR'

        ),
        'langue' => array(
            'variables' => 'fr'
        ),

    );
}



