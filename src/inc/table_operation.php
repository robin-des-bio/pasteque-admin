<?php
///////////////////////////////////////////////////////////////////////////////////
//// Fichier qui regroupe les fonctions pour travailler sur la super variable "table"


function chargement_table()
{
    global $app;
    $pays = (include($app['basepath'] . '/vendor/umpirsky/country-list/data/' . $app['locale_sys'] . '/country.php'));

    setContexte('table', array(
            'category' => charger_table('category'),
            'paymentmode' => charger_table('paymentmode'),
            'role' => charger_table('role'),
            'tax' => charger_table('tax'),
            'place' => charger_table('place'),
            'floor' => charger_table('floor'),
            'currency' => charger_table('currency'),
            'pays' => $pays
        ));

}

function charge_table_specif($table)
{
    global $app;
    $select = 'SELECT *  FROM ' . $table;
    return $app['db']->fetchAll($select);

}


//Renvoi une table id => lib pour choix d'un élément'
function charger_table($objet, $options = array())
{
    global $app;
    $tab_champs = array('*');
    $champs = implode(',', $tab_champs);
    $table = descr($objet . '.table_sql');
    $where = '1=1';
    $orderby = descr($objet . '.choix_ordre');


    $sql = 'SELECT DISTINCT ' . $champs . ' FROM ' . $table . ' WHERE ' . $where . ' ORDER BY ' . $orderby;


    $res = $app['db']->fetchAll($sql);
    $choix = array();
    $cle = descr($objet . '.cle_sql');

    if (isset($options['choix']) and $options['choix']) {
        $tab_champs = array('id_' . $objet, 'nom');
        $cle = 'id_' . $objet;
    } else {
        $tab_champs = array('*');
    }

    if (count($tab_champs) <= 3 && $tab_champs[0] != '*') {

        foreach ($res as $r) {
            $choix[$r[$cle] . ''] = $r['nom'];
        }
    } else {
        foreach ($res as $r) {
            $id = $r[$cle];
            $choix[$id . ''] = $r;
        }
    }

    return $choix;
}




