<?php

function traitement_form_ged($objet_data, $data, $modification,$objet_nom) {
    global $app;

    $tab_fichier=$app['session']->get('file_upload_tmp_' . $objet_nom . '_image');

    if(isset($tab_fichier[0])){
        $ged = GedsQuery::getOneByObjet($objet_nom,$objet_data->getPrimaryKey());
        $modif_ged=true;
        if(!$ged){
            $modif_ged=false;
            $ged=new Geds();
        }else{
            $ged->nettoyage_cache();
        }
        $ged->setFile($app['upload.path'].'/'.$tab_fichier[0]);
        $ged->save();
        $ged_liens = new GedsLiens();
        if(!$modif_ged) {
            $ged_liens->fromArray(array(
                'objet' => $objet_nom,
                'id_objet' => $objet_data->getPrimaryKey(),
                'id_ged' => $ged->getidGed()
            ));
            $ged_liens->save();
    //            arbre($ged_liens);
        }
    //        arbre($ged);
    //        exit;

    }
}


function ged_enregitrer($champs_fichier,$tab_liens=array()){

 global $app;

    $ged=new Geds();
    $fileName = md5(uniqid()).'.'.$champs_fichier->guessExtension();
    $champs_fichier->move( $app['upload.path'],  $fileName  );
    $ged->setFile( $app['upload.path'].'/'.$fileName );

    $ged->save();
    foreach($tab_liens as $objet=>$liens){
        if(!empty($liens)) {

            $liens = is_array($liens)?$liens:[$liens];
            foreach ($liens as $id_objet) {
                $ged_liens = new GedsLiens();
                $ged_liens->fromArray(array(
                    'objet' => $objet,
                    'id_objet' => $id_objet,
                    'id_ged' => $ged->getidGed()
                ));
                $ged_liens->save();


            }
        }
    }
    return true;

}