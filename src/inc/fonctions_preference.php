<?php

use \Propel\Runtime\ActiveQuery\Criteria;
/**
 * pref
 * @param string $var
 * @param string $defaut
 * @return array|null|string
 */
function pref($var = '', $defaut = '')
{
    return lire_preference($var, $defaut);
}


function lire_preference($nom, $defaut = '')
{
    global $app;

    if ($nom && $app['user']) {
        $val_defaut = sac('preference.' . $nom);
        $pref_user = $app['session']->get('preference')[$nom];
        if ($pref_user === null) {
            if ($val_defaut)
                return $val_defaut;
            else
               return $defaut;
        }  else {
            $val = table_merge($val_defaut,$pref_user);
        }
    } else {
        return array();
    }
    if ($val === null) {
        return $defaut;
    }
    return $val;
}






function chargement_preference()
{


    $tab_pref = PreferencesQuery::create()->filterByidIndividu(null,Criteria::ISNULL)->find();
    $tab = array();
    foreach ($tab_pref as $c) {
        $tab[$c->getNom()] = $c->getValeur();
    }

    setContexte('preference', $tab);

}

/**Mise a jour ou création(absence) de l'enregistrement opérateur et/ou entite la création se refere à l'enregistrement maitre en cas d'absense
 * @param $nom sous la forme niveau0[|niveau1[|niveau2]]
 * @param $saisie valeur du dernier niveauX ou tableau complet du dernier niveauX
 *
 * @return bool vrai réussie faux échec todo reste à documenter les erreurs (methode et texte)
 * Exemple : impimante|etisuette et array('nb_colonne' => 3,'nb_ligne' => 9) change 2 valeurs
 * ou impimante|etisuette|nb_colonne et  3  Change 1 valeur
 * ou impimante|etisuette et array('nb_colonne' => 3,'depart' => array('numero'=>4,'autre'=>'test')) change 3 valeurs dont une sous valeur composée de uniquement 2 valeurs si il y en avait plus elle serait perdu
 * todo voir les valeurs ajoutées dans le maitre aprés la création de l'enregistrement spécifique
 */
function ecrire_preference($nom, $value, $entite = false, $operateur = true)
{
    global $app;
    if ($nom) {
        $tab_clef = explode('.', $nom);
        $preference_query = PreferencesQuery::create()->filterByNom($tab_clef[0]);
        if ($operateur) {
            $preference_query->filterByidIndividu(suc('operateur.id'));
        } else {
            $preference_query->filterByidIndividu(null);
        }
        if ($entite) {
            $preference_query->filterByidEntite(pref('en_cours.id_entite'));
        } else {
            $preference_query->filterByidEntite(null);
        }

        $preference = $preference_query->findOne();

        if (!$preference) {//
            $preference = new Preferences();
            $preference->setNom($tab_clef[0]);
            if ($operateur) {
                $preference->setidIndividu(suc('operateur.id'));
            }
            if ($entite){
                $preference->setidEntite(pref('en_cours.id_entite'));
            }
        }
        $preference_valeur = $preference->getValeur();
        array_shift($tab_clef);
        if(empty($tab_clef)){

            $preference->setValeur($value);

        }
        else
        {
            $noeud = implode('.', $tab_clef);
            $preference->setValeur(dessinerUneBranche($preference_valeur, $noeud, $value));
        }

        $preference->save();
        $app['user']->initPreference();
        return true;
    } else {
        return false;
    }
}


/**
 * est utilisé dans les fenetres modales saisie par l'utilisateur n'affiche que les valeurs terminales
 * Todo reste a ajouter les valeurs par défauts combo case a cocher actuellement zone de texte
 * @param $nom sous la forme niveau0[|niveau1[|niveau2]]
 * @param null $id_entite
 * @param int $id_operateur
 * @param bool|true $data_id pour récuperer l'id et nom les data data par défaut False = ID
 * @param int $id si on envoie l'id  les autres parametres sont ignorés
 * @return array c'est un tableau avec les valeurs utilisables par modal
 *
 * pour utilisation voir exemple dans Membre_liste.php et membre_liste.twing
 */
function formulaire_modif_preference($clef, $id_operateur = null, $id_entite = null)
{
    include_once('fonctions_formbuilder.php');


}


/**
 * est utiliser en enregistrement des modales utilisateur
 * et par ecrire_preference : replissage dans la programmation des préférences par exemple
 *
 * @param $valeur tableau du champs Valeur d'une l'intance de l'objet confiq à modifier
 * @param array $clef la clef indique le point de départ des modifications.
 * @param $saisie si saisie est valeur la branche est modifié par la valeur qui peut avoir des sous valeurs
 *                          ou un tableau
 *                                   chaque valeur remplace la branche de la ligne
 *                                   les branches non reseignées ne sont pas affectées.
 */

function modifiePreferenceValeur($valeur, $clef, $saisie)
{

    $clef_z = array_shift($clef);
    if (count($clef) > 0) {
        $valeur[$clef_z] = modifiePreferenceValeur($valeur[$clef_z], $clef, $saisie);
    } else {
        if (empty($clef_z)) {
            return $saisie;
        } else {
            if (is_array($valeur[$clef_z])) {
                foreach ($valeur[$clef_z] as $k => $v) {
                    if (isset($saisie[$k])) {
                        $valeur[$clef_z][$k] = $saisie[$k];
                    }
                }
            } else {
                $valeur[$clef_z] = $saisie;
            }
        }
    }

    return $valeur;
}


// enregistre ou crée l'enregistrement modifié suivant la base et le niveau
function enregistre_preference($objet_data, $id_operateur = null, $id_entite = null, $preference = false)
{
    $entite = null;
    $operateur = null;
    $nom = $objet_data->getNom();
    if ($preference) {// ce n''est pas un générique

        $ok = false;
        if ($id_operateur != $objet_data->getidIndividu() or $id_entite != $objet_data->getidEntite()) {
            $ok = true;
        }
        if ($ok) {
            $objet_data2 = PreferencesQuery::create()->filterByNom($nom)
                ->filterByidEntite($entite)
                ->filterByidIndividu($operateur)
                ->findOne();
            if (isset($objet_data2)) {
                $objet_data2->setValeur($objet_data->getValeur());
            } else {
                $objet_data2 = $objet_data->copy();
                $objet_data2->setidEntite($entite);
                $objet_data2->setidIndividu($operateur);
            }
            $objet_data2->save();
        } else {
            $objet_data->save();
        }
    }
}

