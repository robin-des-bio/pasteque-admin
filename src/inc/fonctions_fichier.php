<?php


function fichier_csv_lecture($fichier, $debut=0, &$fin=100000, $separator=';',$encode_utf8=false)
{
    $tab = array();
    $pointeur_fichier = fopen($fichier,"r+");
    if ($pointeur_fichier !== false) {
        $i=0;
        while($i<$debut){
            fgets($pointeur_fichier, 4096);
            $i++;
        }
        for ($i = 0; $i < $fin-$debut; $i++) {
            $buffer = fgets($pointeur_fichier, 4096);
            if ($encode_utf8)
                $buffer=utf8_encode($buffer);
            if (!empty($buffer))
                $tab[]= explode($separator,$buffer);
            if ( feof($pointeur_fichier)) {
                $fin = $i+$debut;
                break;
            }
        }
        fclose($pointeur_fichier);
    }
    return $tab;
}


function fichier_nb_ligne($fichier){
    $pointeur_fichier = fopen($fichier,"r+");
    fgets($pointeur_fichier,4000);
    $nb=1;
    while(!feof($pointeur_fichier)){
        fgets($pointeur_fichier,4000);
        $nb++;
    }
    return $nb;
}