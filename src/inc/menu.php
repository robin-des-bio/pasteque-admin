<?php



$menu = array(


    'catalog'=>
        array(
            'label'=> 'Catalog',
            'permission'=>'all',
            'menu'=>array(
                'product_list'=>'products',
                'category_list'=>'categories',
                'tax_list'=>'taxes',


            )
        ),
    'costumer'=>
        array(
            'label'=> 'Costumer',
            'permission'=>'all',
            'menu'=>array(
                'customer_list'=>'customer',
                'discount_list'=>'discount',
                'discountcampaigns'=>'discount_campaigns',
                //'place_sales'=>'Place sales'
            )
        ),
    'sales'=>
        array(
            'label'=> 'sales',
            'permission'=>'all',
            'menu'=>array(
                'ticketz'=>'ticketz',
                'sales_by_product'=>'Sales by product',
                'details'=>'Details',
                //'place_sales'=>'Place sales'
            )
        ),
    'administration'=>
        array(
            'label'=> 'Administration',
            'permission'=>'all',
            'menu'=>array(
                'restaurantmap'=>'Restaurant map',
                'cashregister_list'=>'cashregister',
                'currency_list'=>'currency',
                'paymentmode_list'=>'Payment modes',
                'resource_list'=>'Resources',
                'user_list'=>'Users',
                'role_list'=>'Roles',


            )
        ),

    'tools'=>
        array(
            'label'=> 'Tools',
            'permission'=>'all',
            'menu'=>array(
                'memoire'=>'Mémoire',
            )
        ),
/*
    'cashes'=>
        array(
            'label'=> 'administration',
            'permission'=>'all',
            'menu'=>array(
                'cashes'=>'cashes',
                'ztickets'=>'Z tickets',
            )
        ),
*/
    '?'=>
        array(
            'label'=> '?',
            'permission'=>'all',
            'menu'=>array(
                'http://www.pasteque.org'=>'Documentation',
                'credits'=>'Crédits'
            )
        )
);



$app["twig"]->addGlobal("menu_general", $menu);


