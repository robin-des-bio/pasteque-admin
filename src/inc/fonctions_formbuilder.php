<?php


use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormError as FormError;
use Symfony\Component\Validator\Constraints as Assert;


function transforme_data_suivant_entree($data,$tab_champs){


    foreach ($tab_champs as $champs => $valeur) {
        if (is_array($valeur) && isset($valeur['type_champs']))
        {
            if ($valeur['type_champs']=='multiple_cle_valeur'){
                foreach($data[$champs] as $k => &$v)
                    $v = $k.':'.$v;
                $data[$champs]= implode(PHP_EOL,$data[$champs]);
            }
        } elseif(is_array($valeur)){
            $data[$champs] = transforme_data_suivant_entree($data[$champs],$tab_champs[$champs]);
        }
    }

    return $data;
}


function transforme_en_formulaire($nom,$entree,$data,$fonction_enregistrement,$niveau_max=1,$args=array()){

    global $app;
    $ok=false;
    $request=$app['request'];
    if(!is_array($entree)){
        $entree = array($nom=>$entree);
        $data = array($nom=>$data);
    }

    $data = transforme_data_suivant_entree($data,$entree);

    $builder = $app['form.factory']->createNamedBuilder('form_cfpf_'.str_replace('.','_point_',$nom),FormType::class,$data);
    formSetAction($builder,true,$args);
    $builder->setRequired(false);

    $builder = builder_ajoute_champs($builder, $entree,$nom,1,$niveau_max);
    $builder->add('bt_enregistrer',SubmitType::class, array( 'attr' => array('class' => 'btn-primary')));
    $form = $builder->getForm();

    $form->handleRequest($request);
    if ($form->isSubmitted()) {
        $data = $form->getData();
        if ($form->isValid()){
            $ok=true;
            $fonction_enregistrement($nom,$data);
        } else{
            $form->addError(new FormError($app->trans('erreur_saisie_formulaire')));
        }
    }
    return [$form->createView(),$ok];
}



function builder_ajoute_champs($builder, $tab_champs,$nom_complet,$niveau=1,$niveau_max=100) {

    global $app;

    foreach ($tab_champs as $champs => $valeur) {

        if (is_array($valeur) && !isset($valeur['type_champs']))
        {
            if ($niveau<$niveau_max){
                $subform = $app['form.factory']->createNamedBuilder(str_replace('.','_point_',$champs),FormType::class);
                $subform = builder_ajoute_champs($subform, $valeur,$nom_complet,$niveau+1,$niveau_max);
                $builder->add($subform, $champs, array('label' => ' '));
            }
        } else {
            if (is_bool($valeur))
                $type_champs='oui_non';
            elseif (is_array($valeur)){
                $type_champs=$valeur['type_champs'];
                $args=$valeur;
                $valeur=$valeur['valeur'];
            }
            elseif (is_a($valeur,'DateTime')){
                $type_champs = 'date';
            }
            else{
                $type_champs = 'text';
            }

            if (is_array($valeur)){
                $chaine="";
                foreach($valeur as $kv=>$v)
                    $chaine .= $kv.':'.$v.PHP_EOL;
                $valeur = $chaine;
            }

            switch ($type_champs) {
                case 'oui_non':
                    $builder->add($champs, ChoiceType::class, array(
                        // 'constraints' => new Assert\NotBlank(),
                        'label'=>$app->trans($champs),
                        'expanded' => true,
                        'label_attr' => array('class' => 'radio-inline'),
                        'choices' => array('oui' => true, 'non' => false),
                        'attr' => array('inline' => true)));
                    break;

                case 'selection':
                    $choices=['courante'=>'courante'];
                    $tab_select = pref('selection.'.$args['objet']);
                    if(is_array($tab_select)){
                        foreach($tab_select as $i=>$s)
                            $choices[$s['nom']]=$i;
                    }
                    $builder->add($champs, ChoiceType::class, array(
                        'choices' => $choices,
                        'multiple' =>false,
                        'expanded' => false,
                        'attr' => array( )));
                    break;
                case 'textarea'://observations text area
                case 'multiple': //  valeurs
                case 'multiple_cle_valeur': // combo avec couple valeurs M->Monsieur Mme ->Madame
                    $builder->add($champs, TextareaType::class, array(
                        'label'=>$app->trans($champs),
                        'constraints' => new Assert\NotBlank(),
                        'attr' => array( 'rows'=>(isset($args['nb_ligne']))?$args['nb_ligne']:5)));
                    break;
                case 'case_a_cocher':
                     $builder->add($champs, ChoiceType::class, array(
                        'label'=>$app->trans($champs),
                        'choices' => $args['choices'],
                         'multiple' =>true,
                         'expanded' => ($type_champs=='case_a_cocher'),
                        'attr' => array( )));
                    break;
                case 'select':
                case 'radio':
                    $builder->add($champs, ChoiceType::class, array(
                        'choices' => $args['choices'],
                        'expanded' => ($type_champs=='radio'),
                        'label_attr' => array('class' => ($type_champs=='radio')?'radio-inline':''),
                        'attr' => array( )));
                    break;
                case 'date':
                    $builder->add($champs, DateType::class, array(
                        'label'=>$app->trans($champs),
                        'widget' => 'single_text',
                        'format' => 'dd/MM/yyyy',
                        'attr' => array('class' => 'datepickerb')
                    ));
                    break;
                case 'bouton_selection_courante':

                    break;

                default:
                    $builder->add($champs, TextType::class, array(
                      //  'constraints' => new Assert\NotBlank(),
                        'label'=>$app->trans($champs),
                        'attr' => array( )));
                    break;
            }
        }
    }
    return $builder;
}




