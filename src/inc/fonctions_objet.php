<?php


/**
 *
 */
function getSelectionObjet($objet=null,  $params = array(), $tab_cols = null,$liaison=null)
{
    if (!$objet) {
        $objet = sac('objet');
    }
    $nom_class = descr($objet . '.phpname') . 'Query';
    if (method_exists($nom_class,'getWhere')){
        list($from, $where) = $nom_class::getWhere($params);
    }else{
        list($from, $where) = getWhereObjet($objet, $params);
    }
    return getRequeteObjet($objet, $from, $where, $tab_cols,$liaison);
}


/**
 *
 */
function getSelectionObjetNb($objet=null,  $params = array(), $tab_cols = null,$liaison=null)
{
    if (!$objet) {
        $objet = sac('objet');
    }
    $nom_class = 'Pasteque\Admin\Query\\'.descr($objet . '.phpname') . 'Query';

    if (method_exists($nom_class,'getWhere')){
        list($from, $where) = $nom_class::getWhere($params);
    }else{
        list($from, $where) = getWhereObjet($objet, $params);
    }
    return getRequeteObjetNb($objet, $from, $where, $tab_cols,$liaison);
}


/**
 *
 */
function getRequeteObjet($objet, $from, $where, $tab_cols = null,$liaison=null)
{

    if (!empty($liaison)){
        $nom_class = descr($objet . '.phpname') . 'Query';
        if (!is_array($liaison)) $liaison=[$liaison];
        foreach($liaison as $l){
            $o = $nom_class::getTabLiaison($l);
            $pr = descr($objet . '.nom_sql');
            $prl = descr($o['objet'] . '.nom_sql');
            $from[$prl]= descr($o['objet'] . '.table_sql').'  '.$prl;
            $where .= ' AND '.$pr.'.'.$o['local'].'='.$prl.'.'.$o['foreign'];

        }

    }

    if (empty($tab_cols)) {
        $pr = descr($objet . '.nom_sql');
        $cle = descr($objet . '.cle_sql');
        $tab_cols = $pr . '.' . $cle;
    }elseif(is_array($tab_cols)){
        $tab_cols = implode(',',$tab_cols);
    }

    return 'SELECT ' . $tab_cols . ' FROM ' . implode(',', array_values($from)). ' WHERE ' . $where;
}


/**
 *
 */
function getRequeteObjetNb($objet, $from, $where, $tab_cols = null,$liaison=null)
{
    global $app;
    $select = getRequeteObjet($objet, $from, $where, $tab_cols,$liaison);
    $select_count = 'SELECT COUNT(*) as nb_total FROM ' . implode(',',$from) . ' WHERE ' . $where;
    return [$select, $app['db']->fetchColumn($select_count)];
}


/**
 *
 */
function getWhereObjet($objet,  $params = array())
{

    $where = "1=1";
    $pr = descr($objet . '.nom_sql');
    $cle = descr($objet . '.cle_sql');
    $tables = array($pr => descr($objet . '.table_sql'));
    $nom_class_query = getNomClassQuery($objet);
    $champs_recherche = (property_exists($nom_class_query,'champs_recherche'))?$nom_class_query::$champs_recherche:['nom'];

    if (!empty($champs_recherche)) {
        // La recherche
        if ($search = request_ou_options('search', $params)) {

            $recherche = trim($search['value']);

            if (!empty($recherche)) {
                $colle = 'AND (';
                if (intval($recherche) > 0) {
                    $where = '(' . $cle . ' = \'' . $recherche . '\'';
                    $colle = 'OR';
                }
                $where .= ' ' . $colle . ' ( ' . $pr . '.' . implode(' like \'%' . $recherche . '%\' OR ' . $pr . '.',
                        $champs_recherche) . ' like \'%' . $recherche . '%\'))';
            }
        }
    }

    if ($mots = request_ou_options('mots', $params)) {
        if (!is_array($mots)) {
            $mots = array($mots);
        }
        $where .= ' and (select count(ml.id_mot) from asso_mots_liens ml where ml.id_mot IN (' . implode(',',
                $mots) . ') and ml.id_objet=' . $pr . '.' . $cle . ' and objet=\'' . $objet . '\')=' . count($mots);
    }

    $restreindre_id = request_ou_options('limitation_id', $params);

    if (!empty($restreindre_id)) {
        $restreindre_id = explode(',', $restreindre_id);
        $where .= ' and ( ';
        foreach ($restreindre_id as $key => $id) {
            if ($key != 0) {
                $where .= ' or ';
            }
            $ids = explode('-', $id);
            if (count($ids) > 1) {
                $where .= '(' . $pr . '.' . $cle . ' BETWEEN ' . $ids[0] . ' AND ' . $ids[1] . ')';
            } else {
                $where .= $pr . '.' . $cle . ' =' . $ids[0];
            }

        }
        $where .= ')';
    }

    foreach ($tables as $k => $t) {
        $from[$k] = $t . ' ' . $k;
    }
    return [$from, $where];
}




/**
 * getAllObjet
 * @param $objet
 * @param $tab_id
 * @param array $order
 * @param int $offset
 * @param int $limit
 * @return mixed
 */
function getAllObjet($objet, $tab_id, $order = array(), $offset = 0, $limit = null)
{

    global $app;
    $objet_class =getNomClassModel($objet);
    $cle_sql = descr($objet.'.cle_sql');

    if ($limit == null) {
        $limit = 10000000000;
    }

    return $app['orm.em']->getRepository($objet_class)->findBy([$cle_sql=>$tab_id],$order,$limit,$offset);

/*
    if (!empty($order) && is_array($order)) {

        foreach ($order as $k => $o) {

            $q->orderBy($k, strtoupper($o));
        }

    }

*/




}


/**
 *
 */
function objet_liste_prep_dataliste($objet = null,$args=[])
{

    if (!$objet) {
        $objet = sac('objet');
    }
    list($tab_id, $nb_total) = objet_liste_dataliste_preselection($objet,$args,true,false);
    $tab = objet_liste_dataliste_selection($objet, $tab_id,tri_dataliste());

    if (objet_liste_dataliste_detection_complement($objet)){
        $tab_data = objet_liste_dataliste_preparation($objet, $tab,false);
        $tab_data = objet_liste_dataliste_complement($objet, $tab,$tab_data);
    }
    else
    {
        $tab_data = objet_liste_dataliste_preparation($objet, $tab);
    }

    return [$tab_data, $nb_total];

}


/**
 * @param null|string $objet
 * @return
 */
function objet_liste_dataliste($objet = null)
{

    if (!$objet) {
        $objet = sac('objet');
    }
    list($tab_id, $nb_total) = objet_liste_dataliste_preselection($objet,[],true,true);
    $tab = objet_liste_dataliste_selection($objet, $tab_id,tri_dataliste());
    if (objet_liste_dataliste_detection_complement($objet)){
        $tab_data = objet_liste_dataliste_preparation($objet, $tab,false);
        $tab_data = objet_liste_dataliste_complement($objet, $tab,$tab_data);
    }
    else
    {
        $tab_data = objet_liste_dataliste_preparation($objet, $tab);
    }
    return objet_liste_dataliste_envoi($tab_data, $nb_total);

}


/**
 *
 */
function objet_liste_dataliste_preselection($objet = null, $args = array(),$tri=true,$limit=true)
{
    global $app;
    if (!$objet) {
        $objet = sac('objet');
    }
    list($sql, $nb_total) = getSelectionObjetNb($objet,$args);
    if ($tri){
        $tab_tri = tri_dataliste();
        $tab_tri_sql = tri_dataliste_sql(descr($objet . '.nom_sql'), $tab_tri);
        $sql .= $tab_tri_sql;
    }
    if ($limit){
        $start = request_ou_options('start',$args);
        $length = request_ou_options('length',$args);
        if ($length)
            $sql .= ' LIMIT ' . (intval($start) + 0) . ',' . $length;
    }

    $tab_id = $app['db']->fetchAll($sql);
    $cle = descr($objet . '.cle_sql');
    $tab_id = table_simplifier($tab_id,$cle);
    return [$tab_id, $nb_total];
}

/**
 *
 */
function getNomClassQuery($objet){

    $nom_class = '\Pasteque\Admin\Query\\'.descr($objet . '.phpname') . 'Query';
    return $nom_class;
}

/**
 *
 */
function getNomClassModel($objet){

    $nom_class = '\Pasteque\Admin\Model\\'.descr($objet . '.phpname') ;
    return $nom_class;
}


/**
 * @param $objet
 * @param $tab_id
 * @param string $tri
 * @param int $offset
 * @param int $limit
 * @return array
 */
function objet_liste_dataliste_selection($objet, $tab_id,$tri='',$offset=0,$limit=25)
{
    $tab = array();
    if (!empty($tab_id)) {
        $nom_class = getNomClassQuery($objet);
        $tab = $nom_class::getAll($tab_id, $tri,$offset,$limit);
    }
    return $tab;
}

/**
 * @param $objet
 * @param $tab_id
 * @param $tri
 */
function getAll($objet,$tab_id, $tri){
 $nom_class = descr($objet . '.phpname') ;

}


/**
 * @param $objet
 * @param $tab
 * @param bool $only_value
 * @return array
 */
function objet_liste_dataliste_preparation($objet, $tab, $only_value = true)
{
    $tab_data = array();
    if (!empty($tab)) {
        $tab_colonnes = donneTabCol($objet);
        $tab_data = datatable_prepare_data($tab, $tab_colonnes, $only_value);
    }
    return $tab_data;
}

/**
 * @param $objet
 * @return bool
 */
function objet_liste_dataliste_detection_complement($objet)
{

    global $app;
    $php = fichier_php($objet, 'liste');
    if ($php!='objet_liste.php') {
        $nom_fonction_liste = $objet . '_liste';
        if (function_exists($nom_fonction_liste)) {
            include_once($app['basepath'] . '/src/' . $php);
        }
        $nom_fonction = $objet . '_dataliste_complement';
        return function_exists($nom_fonction);
    }
    return false;
}


/**
 * @param $objet
 * @param $tab
 * @param $tab_data
 * @param bool $only_value
 * @return array
 */
function objet_liste_dataliste_complement($objet, $tab, $tab_data, $only_value = true)
{
    $nom_fonction = $objet . '_dataliste_complement';
    $tab_data = $nom_fonction($tab, $tab_data);
    if($only_value)
        $tab_data = array_map('array_values',$tab_data);
    return $tab_data;
}

/**
 * @param $tab_data
 * @param $nb_total
 * @return
 */
function objet_liste_dataliste_envoi($tab_data, $nb_total)
{
    global $app;
    return $app->json([
        'draw' => $app['request']->get('draw'),
        'recordsTotal' => $nb_total,
        'recordsFiltered' => $nb_total,
        'data' => $tab_data,
        'error' => ''
    ]);
}


/**
 * @param null $objet
 * @return array
 */
function objet_liste_rechercher($objet=null)
{

    global $app;

    if (!$objet) {
        $objet = sac('objet');
    }
    $cle = descr($objet . '.cle_sql');
    $sous_requete = getSelectionObjet($objet);
    $nom_class_query = getNomClassQuery($objet);
    $tab_data = array();
    $tab_id = $app['db']->fetchAll($sous_requete. ' LIMIT 0,10' );
    $tab_id =table_simplifier($tab_id,$cle);

    $tab = $nom_class_query::getAll($tab_id,[],0,10);

     foreach ($tab as $t) {
        $tab_data[] = [
            'id'=>$t->getId(),
            'text'=>$t->getLabel()];
    }
    return $tab_data;

}


/**
 * @param null $objet
 * @param null $id
 * @return
 */
function charger_objet($objet=null,$id=null)
{
    global $app;
    $objet = ($objet) ? $objet : sac('objet');
    $id = ($id) ? $id : sac('id');
    $nomQuery = getNomClassModel($objet);

    $result= $app['orm.em']->getRepository($nomQuery)->find(['id'=>$id]);
    return $result;
}

