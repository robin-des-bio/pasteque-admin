<?php


function getObjetLog($id = null){


    $tab=array();
    foreach (sac('description') as $key=>$data){
        if (isset($data['objet_action'])) {
            $tab[$data['log']] = $data['objet_action'];
        } else {
            $tab[$data['log']] = $key;
        }
    }
    $tab= $tab + ['ACT' => 'activite',
    'AUT' => 'autorisation',
    'COM' => 'compte',
    'ECR' => 'ecriture',
    'ENT' => 'entite',
    'GED' => 'ged',
    'JOU' => 'journal',
    'PIE' => 'piece',
    'POS' => 'poste',
    'TRE' => 'tresor',
    'IMP' => 'import',
    'CON' => 'config',
    'PRE' => 'prestation'];
    return getValeur($tab,$id);
}





function class_css_action($code){

    $tab = [
        'CRE'=>'bg-green',
        'MOD'=>'bg-orange',
        'SUP'=>'bg-red',
    ];
    return getValeur($tab,$code);
}





function class_css_objet($code,$substition){

    $tab = [
        'IND'=>'fa-user',
        'MEM'=>'fa-users',
        'SR1'=>'fa-eur',
        'PAI'=>'fa-eur',
        'SR2'=>'fa-eur',
        'SR3'=>'fa-eur',
        'SR4'=>'fa-eur',
        'SR5'=>'fa-eur',
        'SR6'=>'fa-eur',
    ];
    return getValeur($tab,$code,null,$substition);
}



function getValeur($tab, $id = null, $champs = null,$substitution='') {
    if ($id!==null) {
        if (isset($tab[$id])) {
            if ($champs) {
                if (isset($tab[$id][$champs])) {
                    $tab[$id][$champs];
                } else {
                    return $substitution;
                }
            } else {
                return $tab[$id];
            }
        } else {
            return $substitution;
        }
    }
    return $tab;
}


function getSuperCache() {
    return array('table');
}

/*lire au lieu de get car getdescription existe dans vendor*/

function liste_des_objets() {

    return [
        'product' => [],
        'tax' => ['table_sql'=>'taxes'],
        'category' => ['table_sql'=>'categories'],
        'currency' => ['table_sql'=>'currencies'],
        'role' => [],
        'user' => [],
        'place' => [],
        'floor' => [],
        'cashregister' => ['phpname'=>'CashRegister'],
        'paymentmode' => ['phpname'=>'PaymentMode'],
        'tariffarea' => [],
        'discount' => [],
        'discountprofile' => ['phpname'=>'DiscountProfile'],
        'customer' => [],
        'resource' => ['cle_sql'=>'label'],
        'session' => ['phpname'=>'CashSession'],
        'sessionpayment' => [],
        'sessiontaxe' => [],
        'sessioncat' => [],
        'sessioncattaxe' => [],
        'sessioncustbalance' => []

    ];
}


