<?php


function synchronisation(){

    list($reponse, $statut, $err) = appelAPI('/api/sync');
    if($statut==200){
        reinit_table();

        $reponse = json_decode(json_encode($reponse), true);

        $tab_process =
            [
                ['cashRegisters','cashregister',['reference'],[]],
                ['paymentmodes','paymentmode',['reference'],['dispOrder'=>'disp_order']],
                ['categories','category',['reference'],['dispOrder'=>'disp_order']],
                ['taxes','tax',[],[]],
                ['products','product',['reference'],['dispOrder'=>'disp_order','tax'=>'tax_id','category'=>'category_id',]],
                ['places','place',[],['floor' => 'floor_id']],
                ['floors','floor',[],['dispOrder'=>'disp_order']],
                ['roles','role',['name'],[]],
                ['users','user',['name'],['role'=>'role_id']],
                ['tariffareas','tariffarea',['reference'],[]],
                ['customers','customer',[],[]],
                ['discounts','discount',[],[]],
                ['discountprofiles','discountprofile',[],[]],
                ['resources','resource',[],[]],
                ['currencies','currency',['reference'],[]]

            ];


        foreach($tab_process as $process){

            $nom_process = $process[0];

            if (isset($reponse[$nom_process])) {
                if($nom_process == 'roles' ){
                    foreach ($reponse['roles'] as &$values) {
                        $values['permissions'] =json_encode($values['permissions']);
                    }
                }
                objectSync($reponse[$nom_process],$process[1],$process[2],$process[3]);
            }
        }

    }else{
        echo('erreur');
        print_r($err);

    }
    return false;
}



function reinit_table(){

    global $app;

    $app['db']->query('DELETE FROM cashregisters');
    $app['db']->query('DELETE FROM products');
    $app['db']->query('DELETE FROM users');
    $app['db']->query('DELETE FROM roles');
    $app['db']->query('DELETE FROM categories');
    $app['db']->query('DELETE FROM taxes');
    $app['db']->query('DELETE FROM currencies');

}


function partialSync($reponse){


}



function objectSync($reponse,$object,$fields_uniq_object=[],$tab_rename=[]){

    global $app;

    $table = descr($object.'.table_sql');
    $primary_key = descr($object.'.cle_sql');
    $colonnes = descr($object.'.colonnes');
    $uniq_fields = empty($fields_uniq_object)?'':','.implode(',',$fields_uniq_object);
    $tab_temp = $app['db']->fetchAll('SELECT '.$primary_key.$uniq_fields.' FROM '.$table);
    $tab=[$primary_key=>[]];
    foreach($fields_uniq_object as $f) {
        $tab[$f][] = '';
    }
    foreach($tab_temp as $temp){
        $tab[$primary_key][] = $temp[$primary_key];
        foreach($fields_uniq_object as $f) {
            $tab[$f][] = $temp[$f];
        }
    }


    foreach ($reponse as $values) {
        if (!in_array($values[$primary_key], $tab[$primary_key])) {

            foreach($fields_uniq_object as $f) {
                if(in_array($values[$f], $tab[$f])){
                    $values[$f].='_copy_'.uniqid();
                }
            }

            foreach($tab_rename as $old => $new){
                $values[$new]=$values[$old];
                unset($values[$old]);
            }

            $values = array_intersect_key($values,$colonnes);
            $app['db']->insert($table,$values);

        }
    }


}
