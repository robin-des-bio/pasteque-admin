<?php


function initialiserVariablesApplication()
{

    global $app;

    $app['name'] = 'Pasteque-Admin';
    $app['version'] = '8.0.0';
    $app['bdd_version'] = '8.0.0';
    $app['config_version'] = '8.0.0';


    //Chemin
    $app['resources.path'] = $app['basepath'] . '/resources';
    $app['vendor.path'] = $app['basepath'] . '/vendor';
    $app['tmp.path'] = $app['basepath'] . '/tmp';
    $app['upload.path'] = $app['tmp.path'] . '/upload';
    $app['output.path'] = $app['tmp.path'] . '/output';
    $app['image.path'] = $app['tmp.path'] . '/image';
    $app['log.path'] = $app['tmp.path'] . '/log';
    // Cache
    $app['cache.path'] = $app['tmp.path'] . '/cache';
    $app['cache.driver'] = array('driver' => 'file', 'cache_dir' => $app['cache.path'] . '/cache');
    $app['cache.driver'] = array('driver' => 'file', 'cache_dir' => $app['cache.path'] . '/cache');

    // Http cache
    $app['http_cache.cache_dir'] = $app['cache.path'] . '/http';
    // Twig cache
    $app['twig.options.cache'] = $app['cache.path'] . '/twig';
    $app['twig.options.cache_tache'] = $app['cache.path'] . '/twig_tache';


    //Session
    $app['session.variable.name'] = 'pasteque';
    $app['session.variable.lifetime'] = 1800;


    //Securité
    $app['securite.cookie.secret_key'] = 'CleTrésCompliqueavecMAJUSCULES et des ? _ " - entre autres';
    $app['securite.cookie.duree_de_vie'] = 3600;


    //Local
    $app['locale'] = 'fr';
    $app['locale_sys'] = 'fr';
    $app['date.timezone'] = 'Europe/Paris';
    $app['session.default_locale'] = $app['locale'];
    $app['translator.messages'] = array(
        $app['locale'] => [$app['basepath'] . '/resources/locales/' . $app['locale_sys'] . '.yml'],
    );


    // Swiftmailer
    $app['swiftmailer.use_spool'] = false;
    $app['swiftmailer.options'] = array(
        'host' => 'smtp.numericable.fr',
        'port' => 25
    );


    $app['email'] = true;
    $app['email_redirect'] = '';
    $app['email_copie'] = '';
    $app['email_from'] = '';
    $app['email_prefixe_sujet'] = '[pasteque] ';


    // Documentation
    $app['documentation.url'] = 'http://pasteque.org/';


    // API
    $app['api_url'] = '';
    $app['api_mot_de_passe'] = 'MotdePasseTresTresCompliqué';


}


function liste_des_pages()
{

    $tab_pages = array(
        'accueil' => array(
            'droits' => array('all')
        ),
        'image' => array(
            'droits' => array('all')
        ),
        'vars_point_js' => array(
            'droits' => array('all')
        ),
        'memoire' => array(
            'source' => 'systeme/',
            'droits' => array('all')),
        'recherche' => array(
            'droits' => array('all')),
        'summary' => ['droits' => ['all']],
        'sales_by_product' => ['droits' => ['all']],
        'sales_by_category' => ['droits' => ['all']],
        'details' => ['droits' => ['all']],
        'taxes_report' => ['droits' => ['all']],
        'place_sales' => ['droits' => ['all']],
        'restaurantmap' => ['droits' => ['all']],
        'resources' => ['droits' => ['all']],
        'cashes' => ['droits' => ['all']],
        'sync' => ['droits' => ['all']],
        'ticketz' => ['droits' => ['all']],
        'credits' => ['droits' => ['all']],
        'preferences' => ['droits' => ['all']],
    );
    return $tab_pages;

}


