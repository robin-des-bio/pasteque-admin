<?php
function tache_ajouter($fonction,$descriptif,$args=[],$date_exec=null){

    if (TachesQuery::create()->filterByStatut(array(0, 1))->filterByFonction('import')->count() == 0) {
        $tache = new Taches();
        if ($date_exec===null)
            $date_exec = new DateTime();
        $tache->fromArray([
            'descriptif' => $descriptif,
            'fonction' => $fonction,
            'args' => $args,
            'statut' => '0',
            'priorité' => '0',
            'date_execution' => $date_exec
        ]);
        $tache->save();
    }

}