<?php

$app->extend('twig', function ($twig, $app) {

    $twig->addExtension(new Twig_Extensions_Extension_Intl());
    $twig->addExtension(new Twig_Extensions_Extension_Date());
    $twig->addGlobal('application_nom', $app['name']);
    $twig->addGlobal('version', $app['version']);
    $twig->addGlobal('email_prefixe_sujet', $app['email_prefixe_sujet']);

    return $twig;
});




