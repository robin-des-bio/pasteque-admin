<?php

use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\Utilisateur;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\Security\Core\Exception\AuthenticationException;


class UserProvider implements UserProviderInterface
{


    public function __construct()
    {

    }


    public function loadUserByUsername($login)
    {
        global $app;
        $is_login = $app['request']->get('login');
        $user = $app['session']->get('user');

     if (($app['session']->get('token_api') && $app['session']->get('user')) ||$is_login ) {


            try {
                 list($token,$statut,$err) = loginAPI('/api/login',['user'=>$app['api_user'],'password'=>$app['api_password']]);
                if(!empty($token) && $statut==200) {
                   $app['session']->set('token_api', $token);
                }
             } catch (ConnectionException $e) {
                        throw new UsernameNotFoundException(sprintf('User "%s" not found.', $app['api_user']), 0, $e);
             }


             list($user,$statut,$err) = appelAPI('/api/user/getByName/'.$login);

            if ($statut == "200" && isset($user->id)){



                list($role,$statut,$err) = appelAPI('/api/role/'.$user->role);

                $user->roles=[$role->name];
                $app['session']->set('user',$user);
            }


        }

        if (empty($user->id) || !is_array($user->roles) || empty($user->roles)) {
                throw new AuthenticationException(sprintf('Vous n\'avez pas les droits suffisants pour accèder au logiciel.', $login));
        }

        return new Utilisateur($user->id, $user->name, $user->password, '',
            $user->roles, $user->name, true, true, true, true);
    }

/*

    public function loadUserByUsername($username)
    {

            $this->ldap->bind($this->searchDn, $this->searchPassword);
            $username = $this->ldap->escape($username, '', LDAP_ESCAPE_FILTER);
            $query = str_replace('{username}', $username, $this->defaultSearch);
            $search = $this->ldap->find($this->baseDn, $query);


        if (!$search) {
            throw new UsernameNotFoundException(sprintf('User "%s" not found.', $username));
        }

        if ($search['count'] > 1) {
            throw new UsernameNotFoundException('More than one user found');
        }

        $user = $search[0];

        return $this->loadUser($username, $user);
    }

    public function loadUser($username, $user)
    {
        $password = isset($user['userpassword']) ? $user['userpassword'] : null;

        $roles = $this->defaultRoles;

        return new User($username, $password, $roles);
    }


*/
    public function refreshUser(UserInterface $user)
    {
        if (!$user instanceof Utilisateur) {
            throw new UnsupportedUserException(sprintf('Instances of "%s" are not supported.', get_class($user)));
        }
        return $this->loadUserByUsername($user->getUsername());
    }


    public function supportsClass($class)
    {
        return $class === 'Symfony\Component\Security\Core\User\Utilisateur';
    }
}
