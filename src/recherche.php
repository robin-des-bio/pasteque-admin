<?php

use \Pasteque\Admin\Query\ProductQuery;

function recherche(){


    global $app;
    $args=['search'=>['value'=>$app['request']->get('search')]];

    list($tab_product,$nb_product) = objet_liste_prep_dataliste('product',$args);

    list($tab_category,$nb_category) = objet_liste_prep_dataliste('category',$args);

    $args_twig=array(
        'nb_product'=>$nb_product,
        'tab_product'=>$tab_product,
        'nb_category'=>$nb_category,
        'tab_category'=>$tab_category
    );
    return $app['twig']->render(fichier_twig(), $args_twig);





}
function action_recherche_json()
{


    global $app;
    $tab_data = [];

    $tab_product =  objet_liste_rechercher('product');
    $tab_cat =  objet_liste_rechercher('category');


    foreach ($tab_product as $product) {

        $tab_data[] = array(
            'objet'=>'product',
            'id'=>$product['id'],
            'text'=>$product['text'],
        );

    }
   foreach ($tab_cat as $cat) {
        $tab_data[] = array(
            'objet'=>'category',
            'id'=>$cat['id'],
            'text'=>$cat['text'],
        );

    }
    return $app->json($tab_data);

}
