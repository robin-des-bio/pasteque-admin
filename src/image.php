<?php

use PHPImageWorkshop\ImageWorkshop;

function image(){
	global $app;

    $request = $app['request'];
    $image= $request->get('document');

    if ($image){

        $fichier=$app['basepath'].'/resources/documents/'.$image;
        if (file_exists($fichier))
        {
            header('Last-Modified: '.gmdate('D, d M Y H:i:s', filemtime($fichier)).' GMT', true, 200);
            header('Content-Length: '.filesize($fichier));
            $path_parts=pathinfo($fichier);
            switch(strtolower($path_parts['extension'])){
                case 'png' :
                    header('Content-Type: image/png');
                    break;
                case 'jpg' :
                    header('Content-Type: image/jpg');
                    break;

            }
            $app->sendFile($fichier);
            exit();
        }

    }else{


        $id_ged= $request->get('id_ged');
        $type= $request->get('type');
        if($id_ged){

            $ged = GedQuery::create()->findPk($id_ged);

        }else
        {
            $type= $request->get('type');
            $objet= $request->get('objet');
            $id_objet= $request->get('id_objet');
            $ged = GedQuery::getOneByObjet($objet,$id_objet);
        }


    if (!$ged)
        return '';

    switch($type){

        case 'miniature' :

            $image_dir = $app['image.path'].'/';
            $nom_fichier = $ged->saveFile($image_dir,"200x200");
            $layer = ImageWorkshop::initFromPath($image_dir.$nom_fichier);
            $layer->resizeInPixel(200, null, true);
            $image = $layer->getResult("ffffff");
            ob_get_clean();
            header('Content-type: image/jpg');
            header('Content-Disposition: filename="'.$ged->getNom().'.jpg');
            imagejpeg($image, null, 95);
            //unlink($image_dir.$nom_fichier);
        break;
        default :
            ob_get_clean();
            header('Content-type: '.getMimeTypes($ged->getExtension()));
            header('Content-Disposition: filename="'.$ged->getNom().'.'.$ged->getExtension().'"');
            $image_dir = $app['tmp.path'].'/';
            $nom_fichier = $ged->saveFile($image_dir);
            return $app->sendFile($app['tmp.path'].'/'.$nom_fichier);
        break;
    }
    }

    exit;
}

