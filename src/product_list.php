<?php

function product_list()
{
    global $app;
    $args_twig = [
        'tab_col' => product_colonnes(),
        'tab_filtres' => product_filtres()
    ];
    return $app['twig']->render(fichier_twig(), $args_twig);
}


function product_colonnes()
{

    $tab_colonne = array();
    $tab_colonne['id'] = ['title' => 'id'];
    $tab_colonne['reference'] = ['title' => 'reference'];
    $tab_colonne['label'] = ['title' => 'label'];
    $tab_colonne['category.label'] = ['title' => 'category', "orderable" => false];
    $tab_colonne['tax.label'] = ['title' => 'tax', "orderable" => false];
    $tab_colonne['priceSellvat'] = ['title' => 'Taxed Price', "orderable" => false];
    $tab_colonne['action'] = ["orderable" => false,'width'=>'110'];
    $tab_colonne = datatable_complete_liste_colonne($tab_colonne);
    return $tab_colonne;
}


function product_filtres()
{
    global $app;
    $request = $app['request'];
    $rechercher = $request->get('search');
    $tab_filtres['premier']['search'] = filtre_ajouter_recherche($rechercher['value']);
    $tab_filtres['premier']['category'] = filtre_ajouter_categorie($rechercher['category']);
    return $tab_filtres;
}


function action_product_list_dataliste()
{
    return objet_liste_dataliste('product');
}


function action_product_list_rechercher()
{
    return objet_liste_rechercher('product');
}

