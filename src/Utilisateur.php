<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Symfony\Component\Security\Core\User;

/**
 * User is the user implementation used by the in-memory user provider.
 *
 * This should not be used for anything else.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 */
final class Utilisateur implements AdvancedUserInterface
{

    private $id;
    private $username;
    private $password;
    private $salt;
    private $enabled;
    private $accountNonExpired;
    private $credentialsNonExpired;
    private $accountNonLocked;
    private $roles;
    private $nom;
    private $token;






    public function __construct($id,$username, $password, $salt, array $roles = array(),$nom="",
                                $enabled = true, $userNonExpired = true, $credentialsNonExpired = true, $userNonLocked = true)
     { global $app;
        if (empty($username)) {
             throw new \InvalidArgumentException('The username cannot be empty.');
        }
            $this->id = $id;
            $this->username = $username;
            $this->password = $password;
            $this->salt = $salt;
            $this->enabled = $enabled;
            $this->accountNonExpired = $userNonExpired;
            $this->credentialsNonExpired = $credentialsNonExpired;
            $this->accountNonLocked = $userNonLocked;
            $this->roles = $roles;
            $this->nom = $nom;

     }

    /**
     * {@inheritdoc}
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * {@inheritdoc}
     */
    public function getRoles()
    {
        return $this->roles;
    }


    /**
     * {@inheritdoc}
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * {@inheritdoc}
     */
    public function getSalt()
    {
		return $this->salt;
    }

    /**
     * {@inheritdoc}
     */
    public function getUsername()
    {
        return $this->username;
    }
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * {@inheritdoc}
     */
    public function isAccountNonExpired()
    {
        return $this->accountNonExpired;
    }

    /**
     * {@inheritdoc}
     */
    public function isAccountNonLocked()
    {
        return $this->accountNonLocked;
    }

    /**
     * {@inheritdoc}
     */
    public function isCredentialsNonExpired()
    {
        return $this->credentialsNonExpired;
    }

    /**
     * {@inheritdoc}
     */
    public function isEnabled()
    {
        return $this->enabled;
    }

    /**
     * {@inheritdoc}
     */
    public function eraseCredentials()
    {
    }


    public function getToken()
    {
        return $this->token;
    }




}
