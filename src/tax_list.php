<?php

function tax_list()
{
    global $app;
    $args_twig = [
        'tab_col' => tax_colonnes(),
        'tab_filtres' => tax_filtres()
    ];
    return $app['twig']->render(fichier_twig(), $args_twig);
}


function tax_colonnes()
{

    $tab_colonne = array();
    $tab_colonne['id'] = ['title' => 'id'];
    $tab_colonne['label'] = ['title' => 'label'];
    $tab_colonne['rate'] = ['title' => 'rate'];
    $tab_colonne['action'] = ["orderable" => false];
    $tab_colonne = datatable_complete_liste_colonne($tab_colonne);
    return $tab_colonne;
}


function tax_filtres()
{
    global $app;
    $request = $app['request'];
    $rechercher = $request->get('search');
    $tab_filtres['premier']['search'] = filtre_ajouter_recherche($rechercher['value']);
    return $tab_filtres;
}


function action_tax_list_dataliste()
{
    return objet_liste_dataliste('tax');
}


function action_tax_list_rechercher()
{
    return objet_liste_rechercher('tax');
}

