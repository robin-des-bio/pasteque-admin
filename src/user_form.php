<?php


use Symfony\Component\Form\FormError as FormError;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Pasteque\Admin\Model\User;
use Pasteque\Admin\Forms\UserForm;


function user_form()
{
    global $app;
    $args_rep = [];
    $request = $app['request'];
    $id = sac('id');
    if ($id) {
        $modification = true;
        $user = charger_objet();
        $data = $user->toArray();

    } else {
        $modification = false;


    }
    $builder = $app['form.factory']->createNamedBuilder('user', UserForm::class, $data);
    $builder->setRequired(false);
    formSetAction($builder, $modification, ['id' => $id]);

    $form = $builder->add('submit', SubmitType::class,
        array('label' => $app->trans('Save'), 'attr' => array('class' => 'btn-primary')))
        ->getForm();


    $form->handleRequest($request);
    if ($form->isSubmitted()) {
        $data_form = $form->getData();
        if ($form->isValid()) {

            if (!$modification) {
                $user = new user();
            }

            $user->fromArray($data_form);

            if ($modification) {
                list($reponse, $statut, $err) = appelAPI('/api/user', [], $user->toStruct(), [], 'POST');
            } else {
                // create a user

            }
            if ($statut == 200) {
                $app['orm.em']->persist($user);
                $app['orm.em']->flush();
            }
        }
    }
    return reponse_formulaire($form, $args_rep);

}


function action_user_form_supprimer()
{
    return action_supprimer_une_instance();
}
