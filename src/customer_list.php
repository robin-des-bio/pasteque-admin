<?php

function customer_list()
{
    global $app;
    $args_twig = [
        'tab_col' => customer_colonnes(),
        'tab_filtres' => customer_filtres()
    ];
    return $app['twig']->render(fichier_twig(), $args_twig);
}


function customer_colonnes()
{

    $tab_colonne = array();
    $tab_colonne['id'] = ['title' => 'id'];
    $tab_colonne['disName'] = [];
    $tab_colonne['firstname'] = [];
    $tab_colonne['addr1'] = [];
    $tab_colonne['addr2'] = [];
    $tab_colonne['ZipCode'] = [];
    $tab_colonne['city'] = [];
    $tab_colonne['country'] = [];
    $tab_colonne['email'] = [];
    $tab_colonne['phone1'] = [];
    $tab_colonne['action'] = ["orderable" => false];
    $tab_colonne = datatable_complete_liste_colonne($tab_colonne);
    return $tab_colonne;
}


function customer_filtres()
{
    global $app;
    $request = $app['request'];
    $rechercher = $request->get('search');
    $tab_filtres['premier']['search'] = filtre_ajouter_recherche($rechercher['value']);
    return $tab_filtres;
}


function action_customer_list_dataliste()
{
    return objet_liste_dataliste('customer');
}


function action_customer_list_rechercher()
{
    return objet_liste_rechercher('customer');
}

