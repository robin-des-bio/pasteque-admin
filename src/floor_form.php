<?php


use Symfony\Component\Form\FormError as FormError;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Pasteque\Admin\Model\Floor;
use Pasteque\Admin\Model\Place;
use Pasteque\Admin\Forms\FloorForm;


function floor_form()
{
    global $app;
    $args_rep = [];
    $request = $app['request'];
    $id = sac('id');
    $data = [];
    if ($id) {
        $modification = true;
        $floor = charger_objet();
        $data = $floor->toArray();

    } else {
        $modification = false;

    }
    $builder = $app['form.factory']->createNamedBuilder('floor',FloorForm::class,$data);
    $builder->setRequired(false);
    formSetAction($builder,$modification);

    $form = $builder->add('submit',SubmitType::class,
                array('label' => $app->trans('Save'), 'attr' => array('class' => 'btn-primary')))
            ->getForm();


    $form->handleRequest($request);
    if ($form->isSubmitted()) {
        $data_form = $form->getData();
        if ($form->isValid()) {

            if (!$modification){
                $floor = new Floor();
            }

            $floor->fromArray($data_form);
            list($reponse, $statut, $err) = appelAPI('/api/floor',[],$floor->toStruct(),[],'PUT');
            if ($statut==200){
                $app['orm.em']->persist($floor);
                $app['orm.em']->flush();
            }
        }
    }
    $args_rep['js_init'] = 'floor_form';
    return reponse_formulaire($form,$args_rep);

}


function action_floor_form_add_place(){
    global $app;
    $ok =false;
    $place = new Place();
    $place->setLabel($app->trans('New table'));
    list($reponse, $statut, $err) = appelAPI('/api/floor',[],$place->toStruct(),[],'DELETE');
    if ($statut==200){
        $ok =true;
        $app['orm.em']->persist($place);
       $app['orm.em']->flush();
    }
    return $app->json(['ok'=>$ok]);
}



function action_floor_form_supprimer(){
    return action_supprimer_une_instance();
}
