<?php

function resource_list()
{
    global $app;
    $args_twig = [
        'tab_col' => resource_colonnes(),
        'tab_filtres' => resource_filtres()
    ];
    return $app['twig']->render(fichier_twig(), $args_twig);
}


function resource_colonnes()
{

    $tab_colonne = array();
    $tab_colonne['label'] = [];
    $tab_colonne['type'] = [];
    $tab_colonne['action'] = ["orderable" => false];
    $tab_colonne = datatable_complete_liste_colonne($tab_colonne);
    return $tab_colonne;
}


function resource_filtres()
{
    global $app;
    $request = $app['request'];
    $rechercher = $request->get('search');
    $tab_filtres['premier']['search'] = filtre_ajouter_recherche($rechercher['value']);
    return $tab_filtres;
}


function action_resource_list_dataliste()
{
    return objet_liste_dataliste('resource');
}


function action_resource_list_rechercher()
{
    return objet_liste_rechercher('resource');
}

