<?php


use Symfony\Component\Form\FormError as FormError;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Pasteque\Admin\Model\Tax;
use Pasteque\Admin\Forms\TaxForm;


function tax_form()
{
    global $app;
    $args_rep = [];
    $request = $app['request'];
    $id = sac('id');
    $data = [];
    if ($id) {
        $modification = true;
        $tax = charger_objet();
        $data = $tax->toArray();

    } else {
        $modification = false;
        $data = ['rate'=>0];

    }
    $builder = $app['form.factory']->createNamedBuilder('tax',TaxForm::class,$data);
    $builder->setRequired(false);
    formSetAction($builder,$modification);

    $form = $builder->add('submit',SubmitType::class,
                array('label' => $app->trans('Save'), 'attr' => array('class' => 'btn-primary')))
            ->getForm();


    $form->handleRequest($request);
    if ($form->isSubmitted()) {
        $data_form = $form->getData();
        if ($form->isValid()) {

            if (!$modification){
                $tax = new tax();
            }
            $tax->fromArray($data_form);

            $app['orm.em']->persist($tax);

            list($reponse, $statut, $err) = appelAPI('/api/tax',[],$tax->toStruct(),[],'PUT');

            $app['orm.em']->flush();
        }
    }
    $args_rep['js_init'] = 'tax_form';
    return reponse_formulaire($form,$args_rep);

}


function action_tax_form_supprimer(){
    return action_supprimer_une_instance();
}
