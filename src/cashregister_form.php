<?php


use Symfony\Component\Form\FormError as FormError;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Pasteque\Admin\Model\Cashregister;
use Pasteque\Admin\Forms\CashregisterForm;


function cashregister_form()
{
    global $app;
    $args_rep = [];
    $request = $app['request'];
    $id = sac('id');
    $data = [];
    if ($id) {
        $modification = true;
        $cashregister = charger_objet();
        $data = $cashregister->toArray();

    } else {
        $modification = false;
    }
    $builder = $app['form.factory']->createNamedBuilder('cashregister',CashregisterForm::class,$data);
    $builder->setRequired(false);
    formSetAction($builder,$modification);

    $form = $builder->add('submit',SubmitType::class,
                array('label' => $app->trans('Save'), 'attr' => array('class' => 'btn-primary')))
            ->getForm();


    $form->handleRequest($request);
    if ($form->isSubmitted()) {
        $data_form = $form->getData();
        if ($form->isValid()) {

            if (!$modification){
                $cashregister = new Cashregister();
            }

            $cashregister->fromArray($data_form);
            list($reponse, $statut, $err) = appelAPI('/api/cashregister',[],$cashregister->toStruct(),[],'PUT');
            if ($statut==200){
                $app['orm.em']->persist($cashregister);
                $app['orm.em']->flush();
            }

        }
    }
    $args_rep['js_init'] = 'cashregister_form';
    return reponse_formulaire($form,$args_rep);

}


function action_cashregister_form_supprimer(){
    return action_supprimer_une_instance();
}
