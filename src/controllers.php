<?php


use JsonRPC\Server;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\Constraints as Assert;


$app->before(function (Request $req) {
    global $app;
    $route = $req->get('_route');
    init_sac();
    if ($app['user']) {
        if (verification_config()) {
            $app['cache']->clear();
            init_sac();
            require_once($app['basepath'] . '/src/inc/config_init.php');
            config_maj();
            verifier_config();
        }
        verification_sync();
    }
});


$app->error(function (\Exception $e, $code) use ($app) {
    if ($app['debug']) {
        $message = $code;
        return;
    }
    switch ($code) {
        case "404":
            $message = 'La page demandée n\'a pu être trouvée.';
            break;
        default:
            $message = 'Oups...c\'est embarrassant, une erreur s\'est produite';
    }
    return new Response($message);
});


$app->match('/erreur_403', function () use ($app) {
    include('erreur_403.php');
    return erreur_403();
})->bind('erreur_403');


$app->match('/erreur_404', function () use ($app) {
    include('erreur_404.php');
    return erreur_404();
})->bind('erreur_404');


$app->match('/', function () use ($app) {

    init_sac();
    require_once($app['basepath'] . '/src/inc/fonctions_twig.php');
    include('inc/menu.php');
    include('accueil.php');
    return accueil();
})->bind('homepage');


$app->match('/login', function (Request $request) use ($app) {

    include('login.php');
    $action = $request->get('action');
    $code_php = 'login';
    if (!empty($action)) {
        $code_php = 'action_login_' . $action;
    }
    return $code_php($request);
})->bind('login');


$app->get('/logout', function () use ($app) {
    $app['session']->delete();
})->bind('user.logout');


$pages = getListePages();

foreach ($pages as $view) {
    $app->match('/' . str_replace('_point_','.',$view), function () use ($app, $view) {

        if (!pageAutorisee()) {
            return $app->redirect('erreur_403');
        }
        require_once($app['basepath'] . '/src/inc/fonctions_twig.php');
        include('inc/menu.php');
        init_sac();

        $code_php = sac('fonction_php');
        include(fichier_php());

        if ($action = $app['request']->get('action')) {
            $code_php = 'action_' . $code_php . '_' . $action;
        }
        return $code_php();
    })->bind($view);
}
return $app;
