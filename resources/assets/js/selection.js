
function initialisation_selection(){

$('#btn_selection').click(function () {
    $('#choix_selection').removeClass('hide');
});
    activerBoutonSelection();


}

function activerBoutonSelection(){



$('#choix_selection div.selection[data-value]').click(function(){

    id = $('#choix_selection').attr('data-rel');
    values = JSON.parse($(this).attr('data-value'));

    $('#filtre_recherche').val('');
    $('.filtres select').each(function (){
        if ($(this).val()!='') {
            $(this).val('').trigger('change.select2')
        }
    });
    for (indice in values) {
        if (indice=='search'){
            $('#filtre_recherche').val(values[indice]['value']);
        }
        else {

            selecteur_filtre = $('.filtres select[name=' + indice + ']');

            if (selecteur_filtre.length == 0)
                selecteur_filtre = $('.filtres select[name=' + indice + '\\[\\]]');

            if (selecteur_filtre.length > 0) {


                if( typeof values[indice] === 'string' ){
                    $(selecteur_filtre).val(values[indice]).trigger('change.select2');
                }
                else
                {
                    if (indice=='commune'||indice=='arrondissement'||indice=='departement'||indice=='region'){

                        ind = Array();
                        for (i in values[indice]) {
                            var newOption = new Option(values[indice][i]['text'], values[indice][i]['id'], true, true);
                            $(selecteur_filtre).append(newOption);
                            ind.push(values[indice][i]['id'])
                        }

                        $(selecteur_filtre).val(ind).trigger('change.select2');
                    }else{

                        $(selecteur_filtre).val(values[indice]).trigger('change.select2');
                    }

                }

            }

        }
    }
    $('#btn_selection_modal').modal('hide');
    table[id].ajax.reload();
});

}


function rafraichirSelection(){

    activerBoutonSelection();
    initialisation_modal_confirm_supprimer('#choix_selection');


}