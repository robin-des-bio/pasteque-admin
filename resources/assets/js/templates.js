(function() {
window["templates"] = window["templates"] || {};

window["templates"]["bouton_action.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '';
with (obj) {
__p += '<div class="btn-group" role="group">\n    {{#url}}\n    <a class="btn btn-info btn-sm force-tooltip " href="{{url}}" title="voir" data-placement="bottom" id="lien_voir">\n        <i class="glyphicon glyphicon-eye-open"></i>\n    </a>\n    {{/url}}\n    {{#url_form}}\n    <a class="btn btn-warning btn-sm force-tooltip modal_form" href="{{url_form}}" title="Modification de la fiche" data-placement="bottom" id="lien_modifier">\n        <i class="fa fa-pencil"></i>\n    </a>\n    {{/url_form}}\n    {{#url_delete}}\n    <a class="btn btn-danger btn-sm force-tooltip " data-confirm-supprimer="Supprimer cette ligne  ? " data-href="{{url_delete}}" href="#" title="supprimer" data-placement="bottom" id="lien_supprimer">\n        <i class="fa fa-trash-o"></i>\n    </a>\n    {{/url_delete}}\n</div>';

}
return __p
}})();