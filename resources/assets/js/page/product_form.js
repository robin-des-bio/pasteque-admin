





function initialisation_product_form(conteneur_form){

    var round_price=0.05;
    var tax_rates = getTax();
    $('input, select').keypress(function(event) { return event.keyCode != 13; });

    $('#product_barcode').change(function() {
        value=$('#product_reference').val()
        if(value=="")
            $('#product_reference').val($(this).val())
    });

    $("input#product_priceSellvat").TouchSpin({

        min: -999999999999999999.99,
        max: 999999999999999999.99,
        step: round_price,
        decimals: 2,
        boostat: 5,
        maxboostedstep: 10,
        decimal:2,
        forcestepdivisibility:'none',
        verticalbuttons: true,
        verticalupclass: 'glyphicon glyphicon-plus',
        verticaldownclass: 'glyphicon glyphicon-minus'

    });

    $("#product_priceBuy").parent('div').after('<div id="li_formedit-margin" class="form-group"><label for="formedit-margin" class="control-label col-sm-3">Taux de marque</label><div class="input-group col-sm-3"><input type="text" placeholder="" class="form-control " value="" name="margin" id="product_margin"><span class="input-group-addon">%</span></div><div class="input-group col-sm-6"><input type="text" placeholder="" class="form-control " value="" name="margin" id="product_marginRate"></div></div>')





    $("#product_margin").change(function() {
        var val = $(this).val().replace(",", ".");
        $(this).val(val);
        var rate = ((val / 100)).toFixed(2);
        $("#product_marginRate").val(rate);
        updateSell(tax_rates,round_price);
        updateMargin();
    });


    $("#product_marginRate").hide();
    $("#product_marginRate").change(function() {
        var val = $(this).val().replace(",", ".");
        $(this).val(val);
        var margin = ((val) * 100).toFixed(2);
        $("#product_margin").val(margin);
        updateSell(tax_rates,round_price);
        updateMargin();
    });

    $("#product_priceSellvat").change(function() {
        var val = $(this).val().replace(",", ".");
        $(this).val(val);
        updateSellPrice(tax_rates);
    });
    $("#product_tax_id").change(function() {
        var round_price='0.05';
        var way_calculation_price ='price';
        if (way_calculation_price =="margin")
            updateMargin();
        else{
            updateSell(tax_rates,round_price);
            updateMargin()
        }
    });
    $("#product_priceSell").change(function() {
        var round_price='0.05';
        var val = $(this).val().replace(",", ".");
        $("#product_realsell").val(val);
        $(this).val(val);
        updateSellVatPrice(tax_rates,round_price);
    });
    $("#product_priceBuy").change(function() {
        var round_price='0.05';
        var way_calculation_price ='price';
        var val = $(this).val().replace(",", ".");
        $(this).val(val);
        if (way_calculation_price =="margin"){
            updateMargin();
        }
        else{
            updateSell(tax_rates,round_price);
            updateMargin();
        }
    });

    updateMargin(tax_rates);
    updateSell(tax_rates,round_price);

    updateBarcode();

    $("#product_barcode").change(updateBarcode);



}





roundPrice = function(number,round_price)
{
    if (round_price > 0.01)
        number=((number/(round_price*100)).toFixed(2)*(round_price*100)).toFixed(2)
    return number
}


bloque_touche_entree=function (event){
    if(event.keyCode == 13)
        event.stop();
}

updateSell = function(tax_rates,round_price) {
    var buy = $("#product_priceBuy").val();
    var rate = $("#product_marginRate").val();
    var sell = buy  / (1 - rate);


    var ratevat = parseFloat(tax_rates[$("#product_tax_id").val()]['rate']);
    var sellvat = sell * ( ratevat+1);
    sellvat = roundPrice(sellvat.toFixed(2),round_price);
    sell = sellvat /( ratevat+1);
    $("#product_priceSellvat").val(sellvat);
    $("#product_realsell").val(sell);
    $("#product_priceSell").val(sell.toFixed(2));
};


updateSellPrice = function(tax_rates) {
    var sellvat = $("#product_priceSellvat").val();
    var rate = parseFloat(tax_rates[$("#product_tax_id").val()]['rate']);
    var sell = sellvat / (rate+1);
    $("#product_realsell").val(sell);
    $("#product_priceSell").val(sell.toFixed(2));
    updateMargin();
};

updateSellVatPrice = function(tax_rates,round_price) {
    // Update sellvat price
    var sell = $("#product_realsell").val();
    var rate = parseFloat(tax_rates[$("#product_tax_id").val()]['rate']);
    var sellvat = roundPrice((sell * (rate+1)),round_price);
    console.log(sell);
    console.log(sell * (rate+1));
    // Round to 2 decimals and refresh sell price to avoid unrounded payments
    //sellvat = sellvat.toFixed(2);
    $("#product_priceSellvat").val(sellvat);
    updateMargin();
};
updateMargin = function() {
    var sell = $("#product_priceSell").val();
    var buy = $("#product_priceBuy").val();
    var ratio = (sell-buy) / sell;
    var margin = (ratio * 100).toFixed(2);
    var rate = (ratio).toFixed(2);
    $("#product_margin").val(margin );
    $("#product_marginRate").val(rate);
};


clearImage = function() {
    $("#img").hide();
    $("#clear").hide();
    $("#restore").show();
    $("#clearImage").val(1);
};
restoreImage = function() {
    $("#img").show();
    $("#clear").show();
    $("#restore").hide();
    $("#clearImage").val(0);
};

updateBarcode = function() {
    var barcode = $("#product_barcode").val();
    if(barcode>999999999999)
        var src = "?{url_action_param}=img&w=barcode&code=" + barcode;
    $("#product_barcodeImg").attr("src", src);
}


generateBarcode = function() {
    var first = Math.floor(Math.random() * 9) + 1;
    var code = new Array();
    code.push(first);
    for (var i = 0; i < 11; i++) {
        var num = Math.floor(Math.random() * 10);
        code.push(num);
    }
    var checksum = 0;
    for (var i = 0; i < code.length; i++) {
        var weight = 1;
        if (i % 2 == 1) {
            weight = 3;
        }
        checksum = checksum + weight * code[i];
    }
    checksum = checksum % 10;
    if (checksum != 0) {
        checksum = 10 - checksum;
    }
    code.push(checksum);
    var barcode = code.join("");
    $("#product_barcode").val(barcode);
    updateBarcode();
};


