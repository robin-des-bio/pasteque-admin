// Include gulp
var gulp = require('gulp');

// Include Our Plugins
var _ = require('lodash');
var async = require('async');
var jshint = require('gulp-jshint');
var sass = require('gulp-sass');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var rename = require('gulp-rename');
var cleanCSS = require('gulp-clean-css');
var iconfont = require('gulp-iconfont');
var consolidate = require('gulp-consolidate');
var template = require('gulp-template-compile');
var googleWebFonts = require('gulp-google-webfonts');
var runTimestamp = Math.round(Date.now() / 1000);

var config = {
    nodeDir: './node_modules',
    dir: './resources/assets',
    dir_js_lib: './resources/assets/js_lib',
    publicDir: './web/assets'
};


gulp.task('sass', function () {
    return gulp.src([
        config.nodeDir + '/jquery-ui/themes/base/jquery-ui.css',
        config.dir + '/sass/bootstrap.scss',
        config.dir + '/sass/app.scss',
        config.dir + '/fonts/fonts.css',
        config.dir+'/font-svg/pastequefont.css',
        config.dir_js_lib + '/plugins/pace/pace.css',
        config.nodeDir + '/font-awesome/css/font-awesome.css',
        config.nodeDir + '/datatables.net-bs/css/dataTables.bootstrap.css',
        config.nodeDir + '/datatables.net-keytable-bs/css/keyTable.dataTables.css',
        config.nodeDir + '/datatables.net-responsive-bs/css/responsive.dataTables.css',
        config.nodeDir + '/datatables.net-scroller-bs/css/scroller.bootstrap.css',
        config.nodeDir + '/datatables.net-select-bs/css/select.bootstrap.css',
        config.dir_js_lib + '/datepicker/datepicker3.css',
        config.nodeDir + '/bootstrap-daterangepicker/daterangepicker.css',
        config.nodeDir + '/select2/dist/css/select2.css',
        config.nodeDir + '/select2-bootstrap-theme/dist/select2-bootstrap.css',
        config.nodeDir + '/bootstrap-switch/dist/css/bootstrap3/bootstrap-switch.css',
        config.nodeDir + '/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.css',
        config.nodeDir + '/jquery-upload-file/css/uploadfile.css',
        config.nodeDir + '/x-editable/dist/bootstrap3-editable/css/bootstrap-editable.css',


    ])
        .pipe(sass({
            includePaths: [config.nodeDir + '/bootstrap-sass/assets/stylesheets/']
        }).on('error', sass.logError))
        .pipe(cleanCSS({compatibility: 'ie8'}))
        .pipe(concat('styles.css'))
        .pipe(gulp.dest(config.publicDir + '/css'));
});


// Concatenate & Minify JS
gulp.task('scripts', function () {
    return gulp.src([

        config.nodeDir + '/jquery/dist/jquery.min.js',
        config.nodeDir + '/jquery-form/src/jquery.form.js',
        config.nodeDir + '/jquery-form/src/jquery.form.js',
        config.nodeDir + '/mustache/mustache.js',
        config.nodeDir + '/moment/moment.js',
        config.nodeDir + '/datatables.net/js/jquery.dataTables.js',
        config.nodeDir + '/datatables.net-bs/js/dataTables.bootstrap.js',
        config.nodeDir + '/datatables.net-plugins/sorting/date-eu.js',
        config.nodeDir + '/datatables.net-keytable/js/keyTable.dataTables.js',
        config.nodeDir + '/datatables.net-responsive/js/responsive.dataTables.js',
        config.nodeDir + '/datatables.net-responsive-bs/js/responsive.dataTables.js',
        config.nodeDir + '/datatables.net-scroller/js/dataTables.scroller.js',
        config.nodeDir + '/datatables.net-select/js/dataTables.select.js',
        config.nodeDir + '/typeahead.js/dist/bloodhound.js',
        config.nodeDir + '/typeahead.js/dist/typeahead.jquery.js',
        config.nodeDir + '/js-cookie/src/js.cookie.js',
        config.dir + '/js/**/*.js'])
        .pipe(concat('scripts.js'))
        //.pipe(uglify())
        .pipe(gulp.dest(config.publicDir + '/js'));
});


gulp.task('scripts_bas_de_page', function () {
    return gulp.src([

        config.dir_js_lib + '/fastclick/fastclick.js',
        config.dir_js_lib + '/pace/pace.js',
        config.dir_js_lib + '/jQueryUI/jquery-ui.js',
        config.nodeDir + '/URI-js/src/URI.js',
        config.nodeDir + '/bootstrap-sass/assets/javascripts/bootstrap.js',
        config.nodeDir + '/bootstrap-switch/dist/js/bootstrap-switch.js',
        config.nodeDir + '/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.js',
        config.dir_js_lib + '/input-mask/jquery.inputmask.js',
        config.dir_js_lib + '/input-mask/jquery.inputmask.date.extensions.js',
        config.dir_js_lib + '/input-mask/jquery.inputmask.extensions.js',
        config.dir_js_lib + '/datepicker/bootstrap-datepicker.js',
        config.dir_js_lib + '/datepicker/locales/bootstrap-datepicker.fr.js',
        config.nodeDir + '/bootstrap-daterangepicker/daterangepicker.js',
        config.nodeDir + '/select2/dist/js/select2.full.js',
        config.nodeDir + '/typeahead.js/dist/bloodhound.js',
        config.nodeDir + '/typeahead.js/dist/typeahead.jquery.js',
        config.nodeDir + '/jquery-upload-file/js/jquery.uploadfile.min.js',
    ])

        .pipe(concat('scripts_footer.js'))
      //  .pipe(uglify())
        .pipe(gulp.dest(config.publicDir + '/js'));
});


gulp.task('fonts', function () {
    return gulp.src([

            config.nodeDir + '/bootstrap-sass/assets/fonts/bootstrap/*.{ttf,woff,woff2,eof,svg}',
            config.nodeDir + '/font-awesome/fonts/*.{ttf,woff,woff2,eof,svg}',
            config.dir + '/fonts/*.{ttf,woff,woff2,eof,svg}'
        ]
    )
        .pipe(gulp.dest(config.publicDir + '/fonts'));

});


gulp.task('fonts2', function () {
    return gulp.src('./fonts.list')
        .pipe(googleWebFonts({}))
        .pipe(gulp.dest(config.dir+'/fonts'));
});


gulp.task('copie_fichier', function () {
    /*return  gulp.src([
     config.nodeDir+'/datatable-plugins/i18n/French.lang'
     ])
     .pipe(concat('french.lang'))
     .pipe(gulp.dest(config.publicDir+'/js'));*/
    return true;
});


gulp.task('iconfont', function (done) {
    var iconStream = gulp.src([config.dir + '/font-svg/*.svg'])
        .pipe(iconfont({
            fontName: 'pastequefont',
            prependUnicode: true,
            timestamp: runTimestamp,
            normalize: true,
            formats: ['ttf', 'eot', 'woff', 'woff2', 'svg']
        }));

    async.parallel([
        function handleGlyphs(cb) {
            iconStream.on('glyphs', function (glyphs, options) {
                gulp.src(config.dir + '/font-svg/template-font.css')
                    .pipe(consolidate('lodash', {
                        glyphs: glyphs,
                        fontName: 'pastequefont',
                        fontPath: '../fonts/',
                        className: 'ico',

                    }))
                    .pipe(rename("pastequefont.css"))
                    .pipe(gulp.dest(config.dir + '/font-svg'))
                    .on('finish', cb);
            });
        },
        function handleFonts(cb) {
            iconStream
                .pipe(gulp.dest(config.publicDir + '/fonts'))
                .on('finish', cb);
        }
    ], done);

});


gulp.task('templates', function () {
    gulp.src(config.dir + '/templates/**/*.html')
        .pipe(template({namespace: 'templates'}))
        .pipe(concat('templates.js'))
        .pipe(gulp.dest(config.dir + '/js/'));
});


// Default Task
gulp.task('default', ['templates', 'copie_fichier', 'sass', 'scripts', 'scripts_bas_de_page', 'fonts'], function () {

    // Watch For Changes To Our JS
    gulp.watch(config.dir + '/js/**/*.js', ['scripts']);

    // Watch For Changes To Our SCSS
    gulp.watch(config.dir + '/sass/**/*.scss', ['sass']);


});





